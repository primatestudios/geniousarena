using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Jobs;
using UnityEngine;

public class DamageCalculatorManager : MonoBehaviour
{
    [Tooltip("In reference to streaming assets")]
    public string calculatorBinary = "Binaries/calculator_winx64.exe";
    // Start is called before the first frame update
    //NativeArray
    CommandExecutor commandExecutor;
    void Awake()
    {
        calculatorBinary = "\"" + Path.Combine(Application.streamingAssetsPath, calculatorBinary).Replace('/', Path.DirectorySeparatorChar) +"\"";
        Debug.Log("Path: " + calculatorBinary);
        CommandExecutor.InstantiateNewCommandExecutor(
            calculatorBinary,
            (x) =>
            {
                //Debug.Log("Output: " + x);
            },
            (x) => {
                Debug.LogError("Sm4shCalculator Error: " + x);
            },
            (x) =>
            {
                Debug.Log("Sm4shCalculator Exited. Code: " + x);
            }
        );
    }
}