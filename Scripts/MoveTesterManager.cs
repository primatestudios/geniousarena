using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Animations;
using UnityEngine;

[RequireComponent(typeof(MouseOrbit))]
public class MoveTesterManager : MonoSingleton<MoveTesterManager>
{
    public Material hitboxMaterial;
    AnimatorController animatorEditorController;
    FighterData[] fighters;
    FighterData selectedFighterData;
    FighterModel loadedFighter;
    Animator activeAnimator;
    string currentMoveName;
    public void Awake()
    {
       fighters = Resources.LoadAll<FighterData>("");
    }
    public void OnGUI()
    {
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Choose Fighter: ");
        foreach(var fighter in fighters)
        {
            if (GUILayout.Button(fighter.displayName))
            {
                selectedFighterData = fighter;
                if (loadedFighter != null)
                {
                    DestroyImmediate(loadedFighter.gameObject);
                    currentMoveName = null;
                    activeAnimator = null;
                }
            }
        }
        GUILayout.EndHorizontal();

        if (selectedFighterData != null)
        {
            GUILayout.Label("Selected Fighter: " + selectedFighterData.displayName);
            
            if (loadedFighter == null)
            {
                LoadFighter(selectedFighterData);
            }

            DisplayMoves();

        }

        GUILayout.EndVertical();
    }

    void DisplayPlaybackTime()
    {
        GUILayout.BeginHorizontal();
        GUILayout.HorizontalSlider(activeAnimator.playbackTime, 0f, 1f);
        GUILayout.EndHorizontal();
    }

    void DisplaySpeed()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Display Speed: ");
        Time.timeScale = GUILayout.HorizontalSlider(Time.timeScale, 0f, 2f);
        if (GUILayout.Button("Reset"))
        {
            Time.timeScale = 1f;
        }
        GUILayout.EndHorizontal();
    }

    float speed = 1f;
    Vector2 scrollPosition;
    bool filterOnlyAttacks;
    ChildAnimatorState activeState;
    void DisplayMoves()
    {
        DisplaySpeed();
        activeAnimator.speed = speed;

        DisplayPlaybackTime();
        if (currentMoveName != null)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Current Move: " + currentMoveName);
            if (GUILayout.Button("Refresh (After Speed Change)"))
            {
                SwapState(activeState);
            }
            GUILayout.EndHorizontal();
        }
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        AnimatorController ac = selectedFighterData.setupAnimatorController as AnimatorController;
        filterOnlyAttacks = GUILayout.Toggle(filterOnlyAttacks, "Only Display Attacks");
        foreach (var state in states.OrderBy(s => s.state.name))
        {
            string name = state.state.name;
            bool isAttack = false;
            foreach (var b in state.state.behaviours)
            {
                if (b is FighterAnimatorAction && !string.IsNullOrWhiteSpace(((FighterAnimatorAction)b).moveId))
                {
                    isAttack = true;
                    name += "(" + selectedFighterData.GetMove(((FighterAnimatorAction)b).moveId)?.name + ")";
                    break;
                }
            }
            if (((filterOnlyAttacks && isAttack) || !filterOnlyAttacks) && GUILayout.Button(name))
            {
                SwapState(state);
            }
        }
        GUILayout.EndScrollView();
    }

    void SwapState(ChildAnimatorState s)
    {
        activeState = s;
        hitboxes = new Dictionary<string, Smash_Forge.ATKD.HitboxEntry>();
        moveDict = new Dictionary<string, Move>();
        foreach (var m in selectedFighterData.moves)
        {
            hitboxes[m.api_id] = m.hitbox;
            moveDict[m.api_id] = m;
        }

        animatorEditorController = new AnimatorController();
        animatorEditorController.AddLayer("Main Layer");
        ChildAnimatorState cas = new ChildAnimatorState();
        cas.state = new AnimatorState();
        cas.state.speed = s.state.speed;
        cas.state.name = s.state.name;
        cas.state.motion = s.state.motion;
        var exitTransition = cas.state.AddExitTransition();
        exitTransition.hasExitTime = true;
        exitTransition.duration = 0;
        List<StateMachineBehaviour> behaviours = new List<StateMachineBehaviour>();
        foreach(var b in s.state.behaviours)
        {
            if (b is FighterAnimatorAction)
            {
                behaviours.Add(b);
                currentMoveName = ((FighterAnimatorAction)b).moveName;
            }
        }

        cas.state.behaviours = behaviours.ToArray();
        var state = animatorEditorController.layers[0].stateMachine.states = new ChildAnimatorState[] { cas };
        activeAnimator.runtimeAnimatorController = animatorEditorController;
    }

    public Dictionary<string, Move> moveDict;
    public Dictionary<string, Smash_Forge.ATKD.HitboxEntry> hitboxes;

    List<ChildAnimatorState> states;
    void LoadFighter(FighterData fighterData)
    {
        loadedFighter = Instantiate(fighterData.model,Vector3.zero, fighterData.model.transform.rotation);
        loadedFighter.fighterData = selectedFighterData;
        loadedFighter.GetOrAddComponent<HitboxVisualizer>();
        activeAnimator = loadedFighter.gameObject.GetOrAddComponent<Animator>();
        states = GetChildAnimatorStates((fighterData.runtimeAnimatorController as AnimatorController).layers[0].stateMachine);
        GetComponent<MouseOrbit>().target = loadedFighter.transform;
    }

    public List<ChildAnimatorState> GetChildAnimatorStates(AnimatorStateMachine stateMachine)
    {
        List<ChildAnimatorState> states = new List<ChildAnimatorState>();
        foreach (var s in stateMachine.states)
        {
            //    if (s.state.motion != null)
            {
                states.Add(s);
            }
        }

        foreach (var s in stateMachine.stateMachines)
        {
            states = states.Concat(GetChildAnimatorStates(s.stateMachine)).ToList();
        }

        return states;
    }

    public List<AnimatorState> GetAnimatorStates(AnimatorStateMachine stateMachine)
    {
        List<AnimatorState> states = new List<AnimatorState>();

        foreach (var c in GetChildAnimatorStates(stateMachine))
        {
            states.Add(c.state);
        }

        return states;
    }

    public List<AnimatorState> GetAnimatorStates(AnimatorController controller)
    {
        return GetAnimatorStates(controller.layers[0].stateMachine);
    }

    public List<AnimatorStateMachine> GetAnimatorStateMachines(AnimatorController controller)
    {
        return GetAnimatorStateMachines(controller.layers[0].stateMachine);
    }

    public List<AnimatorStateMachine> GetAnimatorStateMachines(AnimatorStateMachine stateMachine)
    {
        List<AnimatorStateMachine> stateMachines = new List<AnimatorStateMachine>();
        foreach (var s in stateMachine.stateMachines)
        {
            stateMachines.Add(s.stateMachine);
            stateMachines = stateMachines.Concat(GetAnimatorStateMachines(s.stateMachine)).ToList();
        }

        return stateMachines;
    }
}
