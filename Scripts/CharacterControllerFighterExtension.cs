using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(CharacterController))]
public class CharacterControllerFighterExtension : MonoBehaviour
{
    FighterController playerController;
    protected void Awake()
    {
        playerController = GetComponentInParent<FighterController>();
    }
    public void OnMovingAnimationStarted()
    {
        playerController.OnMovingAnimationStarted();
    }

    public void OnMovingAnimationEnded()
    {
        playerController.OnMovingAnimationEnded();
    }
}
