using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using static Smash_Forge.ATKD;

[Serializable]
public abstract class SmashCalculatorJSONClass
{
    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}

[Serializable]
public class Sm4shCalculatorLink : SmashCalculatorJSONClass
{
    public string Rel;
    public string Href;
    public string Title;
}

[Serializable]
public class Sm4shCalculatorCharacter : SmashCalculatorJSONClass
{
    public string InstanceId;
    public float Height;
    public string Name;
    public int OwnerId;
    public string FullUrl;
    public string MainImageUrl;
    public string ThumbnailUrl;
    public string ColorTheme;
    public string DisplayName;
    public List<Sm4shCalculatorLink> Links;
}

[Serializable]
public class Sm4shCalculatorCharacterAttributes : SmashCalculatorJSONClass
{
    public string name;
    public float weight;
    public float height;
    public float run_speed;
    public float walk_speed;
    public float air_speed;
    public float fall_speed;
    public float fast_fall_speed;
    public float air_acceleration;
    public float gravity;
    public int sh_air_time;
    public int jumps;
    public bool wall_jump;
    public bool wall_cling;
    public bool crawl;
    public bool tether;
    public int jumpsquat;
    public float soft_landing_lag;
    public float hard_landing_lag;
    public int fh_air_time;
    public float traction;
    public float gravity2;
    public float air_friction;
    public float initial_dash;
    public float run_acceleration;
    public float run_deceleration;
    public float jump_height;
    public float hop_height;
    public float air_jump_height;
}

[Serializable]
public class Aura : SmashCalculatorJSONClass
{
    public string stock_dif;
    public string game_mode;
    public Aura(int stockDifference)
    {
        stock_dif = stockDifference.ToString();
        game_mode = "Singles";
    }
}

[Serializable]
public class OldKnockbackModifier
{
    public string name { get; set; }
    public int damage_dealt { get; set; }
    public int damage_taken { get; set; }
    public int kb_dealt { get; set; }
    public int kb_received { get; set; }
    public int gravity { get; set; }
    public int fall_speed { get; set; }
    public int shield { get; set; }
    public int air_friction { get; set; }
    public int traction { get; set; }
}

[Serializable]
public class Attacker : SmashCalculatorJSONClass
{
    public string character;
    public int character_id;
    public string modifier;
    public double percent;
    public double damage_dealt;
    public double kb_dealt;
    public Aura aura;
    public Attacker(AFighterPhysicsObject smashPhysicsObject, int stockDifference)
    {
        character = smashPhysicsObject.fighterData.displayName;
        character_id = smashPhysicsObject.fighterData.characterId;
        modifier = "Normal";
        percent = smashPhysicsObject.percent;
        damage_dealt = 1;
        kb_dealt = 1;
        aura = new Aura(stockDifference);// attacker.aura;
    }

    public Attacker(AFighterPhysicsObject smashPhysicsObject) : this(smashPhysicsObject, 0) { }
}



[Serializable]
public class Target : SmashCalculatorJSONClass
{
    public string character;
    public int character_id;
    public string modifier;
    public double percent;
    public double weight;
    public double damage_taken;
    public double kb_received;
    public double gravity;
    public double fall_speed;
    public double traction;
    public double luma_percent;

    public Target(AFighterPhysicsObject smashPhysicsObject)
    {
        character = smashPhysicsObject.fighterData.displayName;
        character_id = smashPhysicsObject.fighterData.characterId;
        modifier = "Normal";
        percent = smashPhysicsObject.percent;
        weight = smashPhysicsObject.fighterData.attributes.weight;
        damage_taken = 1;
        kb_received = 1;
        gravity = smashPhysicsObject.fighterData.attributes.gravity;
        fall_speed = smashPhysicsObject.fighterData.attributes.fall_speed;
        traction = smashPhysicsObject.fighterData.attributes.traction;
        luma_percent = 0;
    }
}

[Serializable]
public class Attack : SmashCalculatorJSONClass
{
    public object name;
    public double base_damage;
    public int hitlag;
    public float angle;
    public float bkb;
    public float wbkb;
    public float kbg;
    public float shield_damage;
    public float preLaunchDamage;
    public bool is_smash_attack;
    public int charged_frames;
    public bool windbox;
    public bool projectile;
    public bool set_weight;
    public bool aerial_opponent;
    public bool ignore_staleness;
    public bool mega_man_fsmash;
    public bool on_witch_time;
    public bool unblockable;
    public List<bool> stale_queue;
    public object effect;

    public Attack(Move m, int chargedFrames, AFighterPhysicsObject target)
    {
        name = m.name;
        base_damage = m.base_damage;
        angle = m.angle;
        bkb = m.bkb;
        wbkb = m.wbkb;
        kbg = m.kbg;
        shield_damage = m.shieldDamage;
        preLaunchDamage = m.preLaunchDamage;
        is_smash_attack = m.smash_attack;
        charged_frames = chargedFrames;
        windbox = m.windbox;
        //        set_weight = m.weightDependent
        aerial_opponent = !target.m_IsGrounded;// opponentInAir;
        unblockable = m.unblockable;
        //IGNORING STALENESS FOR NOW
    }
}

[Serializable]
public class Position : SmashCalculatorJSONClass
{
    public float x;
    public float y;
}

[Serializable]
public class KnockbackRequest : SmashCalculatorJSONClass
{
    public NewKnockbackRequest attemptedRequest;
    public Attacker attacker;
    public Target target;
    public Attack attack;
    public Modifiers modifiers;
    public ShieldAdvantage shield_advantage;
    public Stage stage;
    public bool vs_mode;
    [System.NonSerialized] public AFighterPhysicsObject targetController;
    [System.NonSerialized] public AFighterPhysicsObject attackController;
    Move move;

    public KnockbackRequest(Move _move, int chargedFrames, FighterController _attackController, FighterController _targetController)
    {
        attemptedRequest = new NewKnockbackRequest(_move, _attackController, _targetController);
        move = _move;
        vs_mode = true;
        attack = new Attack(_move, chargedFrames, _targetController);
        attacker = new Attacker(_attackController);
        target = new Target(_targetController);
        modifiers = new Modifiers();
        shield_advantage = new ShieldAdvantage();
        targetController = _targetController;
        attackController = _attackController;
    }

    public void OnKnockbackResponse(KnockBackResponse response)
    {
        targetController.HandleKnockbackResponse(response, attackController, move);
    }
}

[Serializable]
public class LaunchSpeed : SmashCalculatorJSONClass
{
    public double x;
    public double y;
}
[Serializable]
public class LaunchTrajectory : SmashCalculatorJSONClass
{
    public int frame;
    public Position position;
    public LaunchSpeed launch_speed;
    public Vector2 vector { get { return new Vector2(position.x, position.y); } set { position.x = value.x; position.y = value.y; } }
}

[Serializable]
public class KnockBackResponse : SmashCalculatorJSONClass
{
    public float damage;
    public object effect_time;
    public int attacker_hitlag;
    public int target_hitlag;
    public object paralysis_time;
    public object flower_time;
    public object buried_time;
    public object sleep_time;
    public object freeze_time;
    public object stun_time;
    public object disable_time;
    public double kb;
    public int launch_angle;
    public double kb_x;
    public double kb_y;
    public int hitstun;
    public int hitstun_faf;
    public int airdodge_hitstun_cancel;
    public int aerial_hitstun_cancel;
    public bool tumble;
    public bool reeling;
    public object reeling_hitstun;
    public object reeling_faf;
    public bool can_jab_lock;
    public int lsi;
    public double horizontal_launch_speed;
    public int gravity_boost;
    public double vertical_launch_speed;
    public double max_horizontal_distance;
    public double max_vertical_distance;
    public int kb_modifier;
    public int rage;
    public int kb_received;
    public int launch_rate;
    public int kb_dealt;
    public object aura;
    public int charged_smash_damage_multiplier;
    public int damage_taken;
    public int damage_dealt;
    public int preLaunchDamage;
    public double stale_negation;
    public int hit_advantage;
    public bool unblockable;
    public float shield_damage;
    public int full_shield_hp;
    public bool shield_break;
    public int shield_hitlag;
    public int shield_stun;
    public int shield_advantage;
    public double attacker_pushback;
    public double target_pushback;
    public object luma_kb;
    public object luma_launched;
    public object luma_hitstun;
    public bool ko;
    public List<LaunchTrajectory> launch_trajectory;
}

[Serializable]
public class Charge : SmashCalculatorJSONClass
{
    public List<string> names;
    public int min;
    public int max;
    public bool variable_bkb;
}

[Serializable]
public class MoveLink : SmashCalculatorJSONClass
{
    public string Rel { get; set; }
    public string Href;
}