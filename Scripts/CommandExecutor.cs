using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public class CommandExecutor : MonoBehaviour
{
    public void StandardIn(string value)
    {
        if (process == null)
        {
            UnityEngine.Debug.LogError("Cannot send data to standard in - process does not exist: [" + value + "]");
        }
    }

    public void KillProcess()
    {
        if (process != null)
        {
            KillChildProcesses();
            KillThisProcess();
        }
    }

    void KillChildProcesses()
    {
        ProcessStartInfo processInfo;
        Process p;
        processInfo = new ProcessStartInfo("cmd.exe", "/c wmic process where (ParentProcessId=" + process.Id + ") get ProcessId");
        processInfo.CreateNoWindow = true;
        processInfo.UseShellExecute = false;
        // *** Redirect the output ***
        processInfo.RedirectStandardError = true;
        processInfo.RedirectStandardOutput = true;

        p = Process.Start(processInfo);
        p.WaitForExit();

        // *** Read the streams ***
        // Warning: This approach can lead to deadlocks, see Edit #2
        string output = p.StandardOutput.ReadToEnd();
        string error = p.StandardError.ReadToEnd();
        p.Close();

        foreach(var l in output.Split('\n'))
        {
            int id;
            if (int.TryParse(l, out id))
            {
                Process.GetProcessById(id)?.Kill();
            }
        }
    }

    void KillThisProcess()
    {
        process.StandardInput.Close();
        process.Kill();
        process = null;
    }

    Process process;
    public static CommandExecutor InstantiateNewCommandExecutor(string command, Action<string> OnOutput, Action<string> OnError, Action<int> OnExit)
    {
        GameObject newGo = new GameObject("Command Executor");
        var ret= newGo.AddComponent<CommandExecutor>();
        ret.Execute(command, OnOutput, OnError, OnExit);
        return ret;
    }

    Action<int> OnExit;
    public void Execute(string command, Action<string> OnOutput, Action<string> OnError, Action<int> _OnExit)
    {
        OnExit = _OnExit;
        process = new Process();
        // Log?.Invoke("CommandPrompt: " + commandPromptApplication);
        process.StartInfo.FileName = "cmd.exe";
        process.StartInfo.RedirectStandardInput = true;
        process.StartInfo.RedirectStandardOutput = true;
        process.StartInfo.RedirectStandardError = true;
        process.StartInfo.CreateNoWindow = true;
        process.StartInfo.UseShellExecute = false;
        process.Start();
        //var cmd = bashCommand ? "C:\\Windows\\System32\\bash.exe -c \"" + command + "\"" : command;
        var cmd = command;
        process.StandardInput.WriteLine(cmd);// commandRequest.command);
        process.StandardInput.Flush();
        UnityEngine.Debug.Log("Processing Request (" + process.Id + "): " + cmd);

        //  commandRequest.processId = process.Id;
        //processes[process.Id] = process;
        process.OutputDataReceived += (object sender, System.Diagnostics.DataReceivedEventArgs e) =>
        {
            if (!string.IsNullOrEmpty(e.Data))
            {
                OnOutput?.Invoke(e.Data);
            }
        };

        process.BeginOutputReadLine();
        process.ErrorDataReceived += (object sender, System.Diagnostics.DataReceivedEventArgs e) =>
        {
            if (!string.IsNullOrEmpty(e.Data))
            {
                OnError?.Invoke(e.Data);
            }
        };
        process.BeginErrorReadLine();
    }

    public void Update()
    {
        if (process != null && process.HasExited)
        {
            Exit();
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            KillProcess();
        }
    }

    void Exit()
    {
        CloseProcess();
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            Destroy(gameObject); //Needs to be run from Unity thread
        });
    }

    void CloseProcess()
    {
        if (process != null)
        {
            process.StandardInput.Close();
            ((Action<int>)OnExit)?.Invoke(process.ExitCode);
            process.Close();
            process = null;
        }
    }

    void OnDestroy()
    {
        KillProcess();
    }
}