using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnRightStick : MonoBehaviour
{
    public float sensitivity;

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("CameraX");
        transform.SetYLocalRotation(transform.localRotation.eulerAngles.y + x * sensitivity * Time.deltaTime);
    }
}
