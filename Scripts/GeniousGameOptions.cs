using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeniousGameOptions : Options<GeniousGameOptions>
{
    public int frameRate = 60;
    public float horizontalSmashUnitScalar = 5;
    public float verticalSmashUnitScalar = 5;
    public float airMovementScalar = 5;
    public float rollSmashUnitScalar = .7f;
}