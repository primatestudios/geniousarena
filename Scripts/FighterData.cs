using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Smash_Forge;
using System.Text.RegularExpressions;
//using UnityEditor.Animations;

[Serializable]
public class FighterData : ScriptableObject
{
    public string characterName { get { return characterClass.Name; } }
    public string displayName { get { return characterClass.DisplayName; } }
    public RuntimeAnimatorController runtimeAnimatorController;
    public RuntimeAnimatorController setupAnimatorController;
    public FighterModel model;
    public Avatar avatar;
    public Sprite portrait;
    public string boneRoot = "TransN";
    public int characterId { get { return characterClass.OwnerId; } }
    public Sm4shCalculatorCharacter characterClass;
    public Sm4shCalculatorCharacterAttributes attributes;
    public List<UnparsedMove> unparsedMoves;
    public List<Move> moves;
    /// <summary>
    /// This is the atkd file that should be placed in Streaming Assets
    /// </summary>
    public string _hitboxDataFilePath;
    public string hitboxDataFilePath { get { return String.IsNullOrEmpty(_hitboxDataFilePath) ? characterName : _hitboxDataFilePath; } }
#if UNITY_EDITOR
    public UnityEditor.DefaultAsset atkdAsset;
#endif
    [HideInInspector] public ATKD hitboxData;
    Dictionary<string, List<Move>> _moveDict;
    Dictionary<string, List<Move>> moveDict { get {
            if (_moveDict == null)
            {
                InitMoveDict();
            }

            return _moveDict;
        }

        set
        {
            _moveDict = value;
        }
    }
    public void Init()
    {
        InitMoveDict();
        //hitboxData = new ATKD(this);
    }

    void InitMoveDict()
    {
        moveDict = new Dictionary<string, List<Move>>();
        foreach(var m in moves)
        {
            if (!moveDict.ContainsKey(m.api_id))
            {
                _moveDict[m.api_id] = new List<Move>();
            }

            _moveDict[m.api_id].Add( m);
        }
    }

    public List<Move> GetMoves(string id)
    {
        if (moveDict.ContainsKey(id))
        {
            return moveDict[id];
        }

        return null;
    }

    public Move GetMove(string id, int index = -1)
    {
        var moves = GetMoves(id);
        if (moves == null || index >= moves.Count || moves.Count + index < 0)
        {
            return null;
        }

        if (index < 0)
        {
            return moves[moves.Count + index];
        }

        return moves[index];

    }

    public List<ActionFrameData> GetActionData()
    {
        //Could probably use reflection to make this a lil more generic and less necessary to be updated upon changes
        Dictionary<ActionFrameData, Move> data = new Dictionary<ActionFrameData, Move>();
        Dictionary<string, Move> sharedMoves = new Dictionary<string, Move>();
        foreach (var move in moves)
        {
            data[new ActionFrameData
            {
                actionName = move.moveName,
                totalFrames = move.faf,
            }] = move;
        }

        foreach(var move in moves)
        {
            if (move.moveName.Contains(" "))
            {
                string firstWord = move.moveName.Split(' ')[0];
                if (sharedMoves.ContainsKey(firstWord))
                {
                    data[(new ActionFrameData
                    {
                        actionName = move.moveName,
                        totalFrames = sharedMoves[firstWord].faf,
                    })] = move;
                }
            }
        }

        foreach(var pair in data)
        {
            var d = pair.Key;
            //Debug.Log("ACtion: " + d.actionName);
            //TODO - make a bool for things that allow movement (I.E. rolls)
            if (pair.Value.allowMovement || pair.Value.moveName.ToLower().Contains("roll") || pair.Value.aerial)
            {
                d.freezeMovement = false;
            }
        }
        return data.Keys.ToList();
    }
    

    IEnumerable<T> ConvertList<T>(ICollection list) where T : class
    {
        List<T> ret = new List<T>();

        foreach (var i in list)
        {
            if (i is T)
            {
                ret.Add(i as T);
            }
            else
            {
                Debug.Log("Skipped " + i);
            }
        }
        return ret;
    }
}

[Serializable]
public class FrameRange
{
    public Frame minFrame;
    public Frame maxFrame;
    public FrameRange(Frame min, Frame max)
    {
        minFrame = min;
        maxFrame = max;
    }

    public Frame Range()
    {
        return maxFrame.value - minFrame.value;
    }

    public override string ToString()
    {
        return "[" + minFrame.ToString() + "-" + maxFrame.ToString() + "]";
    }
}

[Serializable]
public class GrabFrameData : HitActionFrameData
{

}

[Serializable]
public class DodgeFrameData : ActionFrameData
{
    public FrameRange intangibility;
}

[Serializable]
public class ThrowFrameData : AttackFrameData
{
    public bool weightDependent;
   // [HideInInspector] public new Frame totalFrames = 1;
   // [HideInInspector] public new FrameRange hitboxActive;
}

[Serializable]
public class AirAttackFrameData : AttackFrameData
{
    public Frame landingLag;
    public FrameRange autoCancel;
    public bool meteor;
}

[Serializable]
public class ChargeableAttackFrameData : AttackFrameData
{
    public Motion chargeClip;
    public bool chargeInfinitely;
    [Tooltip("Only used if chargeInfinitely isn't set")]
    public FrameRange chargedFrameRange;
    [Tooltip("Set to a positive value if you'd like the maximum charged damage to be scaled to this value")]
    public float overrideBaseDamageChargeScale = -1;
    [Tooltip("Set to a positive value if you'd like the maximum charged knockback to be scaled to this value")]
    public float overrideBaseKnockbackChargeScale = -1;
    [Tooltip("Set to a positive value if you'd like the maximum charged knockback growth to be scaled to this value")]
    public float overrideBaseKnockbackGrowthChargeScale = -1;
    public float shieldBreakerMultiplier = 1;
}

[Serializable]
public class AttackFrameData : HitActionFrameData
{
    public List<int> baseDamage;
    public List<int> angle;
    public List<int> baseKnockBack;
    public List<int> knockbackGrowth;
    public List<int> bonusShieldDamage;
    public FrameRange comboInputWindow;
    public Frame comboStartFrame;
    public FrameRange intangibility;
}

[Serializable]
public class ShieldFrameData : ActionFrameData
{
    public Motion shieldStarted;
    public Motion shieldHit;
    public Motion shieldDropped;
    public FrameRange lockedIntoShieldRange;
    public FrameRange shieldDropLag;
}

[Serializable]
public class HitActionFrameData : ActionFrameData
{
    public FrameRange hitboxActive;
}

[Serializable]
public class ActionFrameData : ActionData
{
    public Motion clip;
    public Frame totalFrames;
}

[Serializable]

public class ActionData
{
    public string actionName;
    public bool freezeMovement = true;
    public bool freezeRotation;
}

[Serializable]
public class SpecialPerks
{
    public bool wallJump;
    public bool wallCling;
    public bool crawl;
    public bool tether;
}

[Serializable]
public class Frame
{
    public int value { get { return numFrames; } }
    public int numFrames;
    public float seconds { get { return numFrames / 60f; } }

    public Frame(int _numFrames)
    {
        numFrames = _numFrames;
    }

    public Frame(float seconds)
    {
        numFrames = Mathf.CeilToInt(seconds / (1/60f));
    }

    public static bool operator >(Frame f, int b)
    {
        return f.numFrames > b;
    }

    public static bool operator >=(Frame f, int b)
    {
        return f.numFrames >= b;
    }

    public static bool operator <(Frame f, int b)
    {
        return f.numFrames < b;
    }

    public static bool operator <=(Frame f, int b)
    {
        return f.numFrames <= b;
    }

    public static bool operator ==(Frame f, int b)
    {
        return f.numFrames == b;
    }

    public static bool operator !=(Frame f, int b)
    {
        return f.numFrames != b;
    }

    public static Frame operator+(Frame f1, Frame f2)
    {
        return new Frame(f1.value + f2.value);
    }

    public static implicit operator Frame(int num)
    {
        return new Frame(num);
    }

    public override string ToString()
    {
        return numFrames.ToString();
    }
}