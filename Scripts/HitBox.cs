using Gamekit3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class HitBox : APlayerControllerAccessory
{
    public List<HitPoint> orderedHitPoints;
    [HideInInspector] public HitPoint closestHitPoint;
    BoxCollider hitboxCollider;
    MeleeWeapon meleeWeapon
    {
        get
        {
           // if (playerController != null)
                return playerController.meleeWeapon;
            return null;
        }
    }

    public Vector3 size { get { return hitboxCollider.size; } set { hitboxCollider.size = value; } }
    public Vector3 center { get {return hitboxCollider.center; } set { hitboxCollider.center = value; } }
    public void Awake()
    {
        hitboxCollider = GetComponent<BoxCollider>();
        if (orderedHitPoints == null || orderedHitPoints.Count == 0)
        {
            orderedHitPoints = new List<HitPoint>(GetComponentsInChildren<HitPoint>());
        }

        if (orderedHitPoints.Count == 0)
        {
            orderedHitPoints.Add(gameObject.AddComponent<HitPoint>());
        }
    }

    bool canHit;//Only one hit per hitbox actove state

    void OnHit()
    {
        canHit = false;
    }

    public void OnCollisionStay(Collision collision)
    {
        if (!canHit)
        {
            return;
        }
        GeniousDamageable d = collision.collider.gameObject.GetComponentInParent<GeniousDamageable>();
        if (d != null && playerController.m_Damageable != d)
        {
            var contactPoint = collision.contacts[0].point;
            float distance = float.MaxValue;
            foreach(var hitPoint in orderedHitPoints)
            {
                hitPoint.ResetPosition();
                if (Vector3.Distance(hitPoint.transform.position, contactPoint) < distance)
                {
                    closestHitPoint = hitPoint;
                }
            }

            closestHitPoint.position = contactPoint;
            playerController.Attack(d, this, closestHitPoint);
        }
        else if (meleeWeapon != null)
        {
            meleeWeapon.CheckDamage(collision.collider, collision.contacts[0].point);
        }
    }
    public void OnTriggerStay(Collider other)
    {

        if (!canHit)
        {
            return;
        }
        GeniousDamageable d = other.gameObject.GetComponentInParent<GeniousDamageable>();
        if (d != null && playerController.m_Damageable != d)
        {
            float distance = float.MaxValue;
            foreach (var hitPoint in orderedHitPoints)
            {
                hitPoint.ResetPosition();
                if (Vector3.Distance(hitPoint.transform.position, d.transform.position) < distance)
                {
                    closestHitPoint = hitPoint;
                }
            }
            playerController.Attack(d, this, closestHitPoint);
        }
        else if (d == null && other.gameObject.GetComponent<PlayerController>() == null && meleeWeapon != null)
        {
            meleeWeapon.CheckDamage(other, other.transform.position);
        }
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Debug.Log("Hitbox hit " + hit.gameObject, hit.gameObject);
    }
    public Damageable ellenDamageable;
    public void Update()
    {
        if (playerController == null)
        {
            return;
        }
        //Debug.Log(myPlayerController + " and " + myPlayerController?.m_Damageable + " and " + myPlayerController?.meleeWeapon);
        playerController.m_Damageable.isInvulnerable = meleeWeapon != null && (!playerController.canMove);// || !myPlayerController.isInGuard);
        if (ellenDamageable != null)
        {
            ellenDamageable.isInvulnerable |= playerController.m_Damageable.isInvulnerable;
        }
    }

    int frame;
    public void SetActive(bool val)
    {
        if (val)
        {
            canHit = true;
            if (playerController == null)
            {
                Debug.Log("WTF? Why isn't this working? " + playerController);
            }
            playerController.onHitAnimatorCallback += OnHit;
            frame = Time.frameCount;
        }
        else
        {
            if (playerController != null)
            {
                playerController.onHitAnimatorCallback -= OnHit;
            }
            if (frame == Time.frameCount)
            {
                GenericCoroutineManager.instance.RunAfterFrame(() => { hitboxCollider.enabled = (false); });
                return;
            }
        }

        hitboxCollider.enabled = (val);
    }
}
