using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Experimental.Input;
//using UnityEngine.Experimental.Input.Plugins.PlayerInput;
//using UnityEngine.Experimental.Input.Plugins.Users;
using Rewired;
using UnityEngine.SceneManagement;

public class FighterPlayerInput : MonoBehaviour
{
    [Range(1, 2)]
    public int playerNumber = 1;
    public Player rewiredPlayer { get; set; }
    protected Dictionary<string, Action<bool>> onButton = new Dictionary<string, Action<bool>>();
   // protected Dictionary<string, Action> onButtonDown = new Dictionary<string, Action>();
    protected Dictionary<string, bool> buttonsDown = new Dictionary<string, bool>();
    //protected Dictionary<string, int> buttonsTriggered = new Dictionary<string, int>();

    protected Dictionary<string, Action<Vector2>> onAxis2d = new Dictionary<string, Action<Vector2>>();
    protected Dictionary<string, Vector2> axes2d = new Dictionary<string, Vector2>();

    protected Dictionary<string, Action<float>> onAxis1d = new Dictionary<string, Action<float>>();
    protected Dictionary<string, float> axes1d = new Dictionary<string, float>();
    //public Vector2 dpad;

    [SerializeField] public FighterControls controls;
    [HideInInspector] public FighterControls.PlayerActions playerActions;
    bool inputBlocked;
   // public InputDevice device;

    public bool GetButtonValue(InputAction action)
    {
        return IsButton(action);// buttonsDown[action.name];
    }

    public List<InputAction> allButtonActions = new List<InputAction>();
    public List<InputAction> all1dAxisActions = new List<InputAction>();
    public List<InputAction> all2dAxisActions = new List<InputAction>();

    void SetInputBlocked(bool blocked)
    {
        inputBlocked = blocked;
        if (inputBlocked)
        {
            controls.Disable();
        }
        else if (enabled)
        {
            controls.Enable();
        }
    }

    public Vector2 GetVector2(InputAction horizontalAction, InputAction verticalAction)
    {
        if (rewiredPlayer == null)
        {
            return Vector2.zero;
        }
        return rewiredPlayer.GetAxis2D(horizontalAction.id, verticalAction.id);
    }

    public T Get<T>(InputAction action)
    {
        if (typeof(T) == typeof(float))
        {
            if (rewiredPlayer == null)
                return (T)Convert.ChangeType(0, typeof(float));
            return (T)Convert.ChangeType(rewiredPlayer.GetAxis(action.id), typeof(float)); //axes1d[action.name], typeof(float));

        }
        else if (typeof(T) == typeof(bool))
        {
            if (rewiredPlayer == null)
            {
                return (T)Convert.ChangeType(false, typeof(bool));
            }
            return (T)Convert.ChangeType(rewiredPlayer.GetButton(action.id), typeof(bool));//(T)Convert.ChangeType(buttonsDown[action.name], typeof(bool));
        }
        else
        {
            Debug.LogError("We can't handle type " + typeof(T) + " right now");
            return default(T);
        }
    }
    public Dictionary<string, string> vector2Keys = new Dictionary<string, string>();
    public void AppendAction<T>(InputAction action, Action<T> callback, InputAction verticalActionForVector2 = null )
    {
        if (typeof(T) == typeof(Vector2))
        {
            if (verticalActionForVector2 == null)
            {
                Debug.LogError("Need 2 actions for Axis2D");
            }
            else
            {
                string axis2dKey = action.name + "|" + verticalActionForVector2;
                vector2Keys[action.name] = axis2dKey;
                vector2Keys[verticalActionForVector2.name] = axis2dKey;
                if (!onAxis2d.ContainsKey(axis2dKey))
                {
                    onAxis2d[axis2dKey] = callback as Action<Vector2>;
                }
                else
                {
                    onAxis2d[axis2dKey] += callback as Action<Vector2>;
                }
            }
        }
        else if (typeof(T) == typeof(float))
        {
            if (!onAxis1d.ContainsKey(action.name))
            {
                onAxis1d[action.name] = callback as Action<float>;
            }
            else
            {
                onAxis1d[action.name] += callback as Action<float>;
            }
        }
        else if (typeof(T) == typeof(bool))
        {
            if (!onButton.ContainsKey(action.name))
            {
                onButton[action.name] = callback as Action<bool>;
            }
            else
            {
                onButton[action.name] += callback as Action<bool>;
            }
        }
        else
        {
            Debug.LogError("We can't handle type " + typeof(T) + " right now");
        }
    }

    public void AppendComboAction(Action callback, params InputAction[] actions)
    {
        AppendComboAction(callback, true, actions);
    }

    void FixedUpdate()
    {
        if (rewiredPlayer == null)
            return;
        int i = 0;
        ApplyToAllActions((a) =>
        {
            // Debug.Log("NumTimes: " + ++i);
            if (a.type == InputActionType.Button)
            {
                //  Debug.Log(a.name + " - " + a.id);
                if (rewiredPlayer.GetButtonDown(a.id))
                {
                    //  SmashSettings.Log(a.name + " Down");
                }

                /*if (rewiredPlayer.GetButtonDown(a.id) && onButtonDown.ContainsKey(a.name))
                {
                    onButtonDown[a.name]?.Invoke();
                }*/

                if (onButton.ContainsKey(a.name))
                {
                    bool currentButtonValue = rewiredPlayer.GetButton(a.id);
                    if (!buttonsDown.ContainsKey(a.name) || currentButtonValue != buttonsDown[a.name])
                    {
                        onButton[a.name]?.Invoke(currentButtonValue);
                    }
                    buttonsDown[a.name] = currentButtonValue;
                }
            }
            else if (a.type == InputActionType.Axis)
            {
                if (onAxis1d.ContainsKey(a.name))
                {
                    float currentAxisValue = rewiredPlayer.GetAxis(a.id);
                    if (!axes1d.ContainsKey(a.name) || currentAxisValue != axes1d[a.name])
                    {
                        onAxis1d[a.name]?.Invoke(currentAxisValue);
                    }

                    axes1d[a.name] = currentAxisValue;
                }

                if (vector2Keys.ContainsKey(a.name))
                {
                    axis2dToCompleteAfterFrame.Add(vector2Keys[a.name]);//To avoid doing this twice each frame
                    //Debug.Log("Cn't handle axis2d yet");
                }
            }
            else
            {
                Debug.LogError("Can't handle this type of InputAction");
            }
        });
    }

    HashSet<string> axis2dToCompleteAfterFrame = new HashSet<string>();
    IEnumerator MonitorAxis2d()
    {
        while (true)
        {
            foreach (var key in axis2dToCompleteAfterFrame)
            {
                var split = key.Split('|');
                Vector2 currentAxisValue = rewiredPlayer.GetAxis2D(split[0], split[1]);
                if (!axes2d.ContainsKey(key) || currentAxisValue != axes2d[key])
                {
                    onAxis2d[key]?.Invoke(currentAxisValue);
                }
                axes2d[key] = currentAxisValue;
            }
            axis2dToCompleteAfterFrame.Clear();
            yield return new WaitForFixedUpdate();
        }
    }

    Dictionary<string, List<KeyValuePair<InputAction[], Action>>> combos = new Dictionary<string, List<KeyValuePair<InputAction[], Action>>>();
    public void AppendComboAction(Action callback, bool onTriggerOnly, params InputAction[] actions)
    {
        if (onTriggerOnly)
        {
            foreach(var a in actions)
            {
                if (!combos.ContainsKey(a.name))
                {
                    combos[a.name] = new List<KeyValuePair<InputAction[], Action>>();
                }
                combos[a.name].Add(new KeyValuePair<InputAction[], Action>(actions,callback));
            }
        }
        else
        {
            Debug.LogError("Don't yet handle the combo option for onTriggerOnly being false");
        }
    }

    /*protected virtual void OnEnable()
    {
        if (!inputBlocked && controls != null)
        {
            controls.Enable();
        }
    }

    protected virtual void OnDisable()
    {
        controls.Disable();
    }
    */

    IEnumerator Start()
    {
        //rewiredPlayer = ReInput.players.GetPlayer(playerNumber - 1);
        if (controls != null)
        {
            Initialize();
        }

        yield return new WaitForEndOfFrame();
       /* SceneManager.activeSceneChanged += (x, y) =>
        {
            onButton.Clear();
            onAxis1d.Clear();
            onAxis2d.Clear();
            ApplyToAllActions((action) =>
            {
                Action<InputAction.CallbackContext> a;
            });
        };
        */
    }
    // Start is called before the first frame update
    public virtual void Initialize()
    {
        Debug.Log("Input Initialized!");
        playerActions = controls.Player;
        //actions = InputUser.all[0].actions as SmashControls.PlayerActions;
        //        controls.MakePrivateCopyOfActions();
        /*SetupButton(controls.Player.Jab);
        SetupButton(controls.Player.Special);
        SetupButton(controls.Player.Smash);
        SetupButton(controls.Player.Jump);
        SetupButton(controls.Player.Block);
        SetupButton(controls.Player.Grab);
        SetupButton(controls.Player.MoveForward);
        SetupButton(controls.Player.ToggleTargetLock);
        SetupButton(controls.Player.Target);
        SetupButton(controls.Player.Drop);
        SetupButton(controls.Player.ThrowForward);
        SetupButton(controls.Player.ThrowUp);
        SetupButton(controls.Player.DownAir);
        SetupButton(controls.Player.UpAir);
        SetupButton(controls.Player.NeutralAir);
        SetupButton(controls.Player.AirDodge);
        SetupButton(controls.Player.ToggleCamera);
        SetupButton(controls.Player.GetUp);
        /* SetupButton(inputManager.Player.CameraUp);
         SetupButton(inputManager.Player.CameraDown);
         SetupButton(inputManager.Player.CameraLeft);
         SetupButton(inputManager.Player.CameraRight);
        */
        // SetupAxis(inputManager.Player.MoveHorizontal);
        //  SetupAxis(inputManager.Player.MoveVertical);
        /*
        SetupAxis2d(controls.Player.Dpad);
        SetupAxis2d(controls.Player.Movement);
        SetupAxis2d(controls.Player.AltAxis);
        SetupAxis1d(controls.Player.UpDownAxis);
        */
        // InitializePlayerActionCallbacks();
        //controls.Enable();
        StartCoroutine(MonitorAxis2d());

    }

    void ApplyToAllActions(Action<InputAction> action)
    {
        if (ReInput.mapping == null)
        {
            return;
        }
       // Debug.Log("Actions Count: " + ReInput.mapping.Actions.Count);
        foreach(var a in ReInput.mapping.Actions)
        {
            action.Invoke(a);
        }
       /* foreach (var actionMap in actions.actionMaps)
        {
            foreach (var a in actionMap.actions)
            {
            }
        }*/
    }

/*    void InitializePlayerActionCallbacks()
    {
        ApplyToAllActions((action) =>
        {
            if (onButton.ContainsKey(action.name))
            {
                Action<InputAction.CallbackContext> started = (_) =>
                {

                    //    Debug.Log("Button started: " + action.name);
                    if ((!buttonsDown.ContainsKey(action.name) || !buttonsDown[action.name]))
                    {
                        buttonsTriggered[action.name] = SmashSettings.fixedFrame;
                        HandleCombos(action);
                    }
                    GenericCoroutineManager.instance.RunAfterFrame(() =>
                    {
                        if (!buttonsInCombo.Contains(action.name))
                        {
                            // SmashSettings.Debug(action.name + " Set to true");
                            onButton[action.name].Invoke(true);
                        }
                    });
                };

                Action<InputAction.CallbackContext> performed = (_) =>
                {
                    //    Debug.Log("Button Performed: " + _.action.name);
                    GenericCoroutineManager.instance.RunAfterFrame(() =>
                    {
                        if (!IsButton(action))
                        {
                            onButton[action.name]?.Invoke(false);
                        }
                    });
                };

                Action<InputAction.CallbackContext> cancelled = (_) =>
                { 
                    onButton[action.name]?.Invoke(false);
                    //  Debug.Log("Button Cancelled: " + _.action.name);
                };
                action.started += started;
                action.performed += performed;
                action.cancelled += cancelled;
                SceneManager.activeSceneChanged += (x, y) =>
                {
                    action.started -= started;
                    action.performed -= performed;
                    action.cancelled -= cancelled;
                };               
            }
            else if (onAxis1d.ContainsKey(action.name))
            {
                Action<InputAction.CallbackContext> performed = (context) =>
                {
                    onAxis1d[action.name]?.Invoke(context.ReadValue<float>());
                };
                Action<InputAction.CallbackContext> cancelled= (context) =>
                {
                    onAxis1d[action.name]?.Invoke(0);
                };
                action.performed += performed;
                action.cancelled += cancelled;

                SceneManager.activeSceneChanged += (x, y) =>
                {
                    action.performed -= performed;
                    action.cancelled -= cancelled;
                };
            }
            else if (onAxis2d.ContainsKey(action.name))
            {
                Action<InputAction.CallbackContext> cancelled = (context) =>
                {
                    onAxis2d[action.name]?.Invoke(Vector2.zero);
                };

                Action<InputAction.CallbackContext> performed = (context) =>
                {
                    onAxis2d[action.name]?.Invoke(context.ReadValue<Vector2>());
                };

                action.performed += performed;
                action.cancelled += cancelled;

                SceneManager.activeSceneChanged += (x, y) =>
                {
                    action.performed -= performed;
                    action.cancelled -= cancelled;
                };
            }
            else
            {
                Debug.Log("nothing handling " + action.name + " in " + transform.name, this);
            }
        });
    }
    */
    /*
    protected virtual void Update()
    {
        SetMovementAxis();
    }

    void SetMovementAxis()
    {
        Vector2 newVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        if (axes1d.Count < 1)
        {
            axes1d.Add(newVector);
        }
        else
        {
            axes1d[0] = newVector;
        }
    }*/

    public bool IsButton(InputAction action)
    {
        if (rewiredPlayer == null)
            return false;
        return rewiredPlayer.GetButton(action.id);
//        return action.phase != InputActionPhase.Waiting && action.phase != InputActionPhase.Cancelled && action.phase != InputActionPhase.Cancelled;
    }

    HashSet<string> buttonsInCombo = new HashSet<string>();

    bool HandleCombos(InputAction inputAction)
    {
        Debug.Log("Can't handle combos yet");
        /*
        if (!combos.ContainsKey(inputAction.name))
        {
            return false;
        }

        if (buttonsInCombo.Contains(inputAction.name))
        {
            return true;
        }

        foreach (var pair in combos[inputAction.name])
        {
            string comboString = "";
            bool comboFound = true;
            foreach (var action in pair.Key)
            {
                if (!buttonsTriggered.ContainsKey(action.name) || buttonsTriggered[action.name] != SmashSettings.fixedFrame)
                {
                    comboFound = false;
                    break;
                }

                comboString += " " + action.name + " +";
            }

            if (comboFound)
            {
                foreach (var action in pair.Key)
                {
                    buttonsInCombo.Add(action.name);
                }
                comboString = comboString.Substring(0, comboString.Length - 1);
                //SmashSettings.Log("Combo Triggerrd: " + comboString);
                pair.Value.Invoke();
                return true;
            }
        }*/
        return false;
    }
    /*
    void SetupButton(InputAction action) 
    {
        try
        {
            onButton[action.name] = (value) => {
                //Debug.Log(action.name + ": " + value);
                if (!value)
                {
                    buttonsInCombo.Remove(action.name);
                }
                buttonsDown[action.name] = value;
            };

            buttonsDown[action.name] = false;
            allButtonActions.Add(action);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    void SetupAxis1d(InputAction action)
    {
        try
        {
            onAxis1d[action.name] = (value) => { //Initialize this action for safety
                axes1d[action.name] = value;
            };

            axes1d[action.name] = 0;//Initialization
            all1dAxisActions.Add(action);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    Dictionary<InputAction, int> lastFrameActionAccessed = new Dictionary<InputAction, int>();
    
    /*void SetupAxis2d(InputAction action)
    {
        try
        {
            onAxis2d[action.name] = (value) => {
                axes2d[action.name] = value;
            };

            axes2d[action.name] =Vector2.zero;//Initialization
            all2dAxisActions.Add(action);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }*/
}