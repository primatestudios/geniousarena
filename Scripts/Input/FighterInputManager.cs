using Rewired;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class FighterInputManager : MonoSingleton<FighterInputManager>
{
    // public SmashControls controls;

    //public Rewired.InputAction Jump;
    public FighterPlayerInput startingInactivePlayer;
    public UnityEvent onInactivePlayerActivated;
    public List<FighterPlayerInput> players = new List<FighterPlayerInput>();
    protected override void Awake()
    {
        base.Awake();
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            if (players.Count <= 0)
            {
                players = new List<FighterPlayerInput>(FindObjectsOfType<FighterPlayerInput>());
            }

            players = players.OrderByDescending(x => x.playerNumber).ToList();
        });
        unassignedPlayers = new HashSet<Player>(ReInput.players.AllPlayers);
    }

    private void Update()
    {
        if (players.Count <= 0) return;
        AssignJoysticksToPlayers();
    }

    HashSet<Player> unassignedPlayers = new HashSet<Player>();

    private void AssignJoysticksToPlayers()
    {
        // Check all joysticks for a button press and assign it tp
        // the first Player foudn without a joystick
        IList<Rewired.Joystick> joysticks = ReInput.controllers.Joysticks;
        foreach(var player in unassignedPlayers)
        {
            bool buttonDown = false;
            foreach(var joystick in player.controllers.Joysticks)
            {
                buttonDown = joystick.GetAnyButtonDown();
            }

           // buttonDown |= (player.controllers.hasKeyboard && player.controllers.Keyboard.GetAnyButtonDown());
            if (buttonDown)
            {
                // Find the next Player without a Joystick

                // Assign the joystick to this Player
                //player.controllers.AddController(joystick, false);
               // Debug.Log("aSSIGNING CONTROLLER TO player " + players[players.Count - 1].playerNumber);
                var playerInput = players[players.Count - 1];
                playerInput.rewiredPlayer = player;
                playerInput.gameObject.SetActive(true);
                if (playerInput == startingInactivePlayer)
                {
                    onInactivePlayerActivated.Invoke();
                }
                players.RemoveAt(players.Count - 1);
                GenericCoroutineManager.instance.RunAfterFrame(() =>
                {
                    unassignedPlayers.Remove(player);
                });
            }
        }

        return;
        for (int i = 0; i < joysticks.Count; i++)
        {
            Joystick joystick = joysticks[i];
            if (ReInput.controllers.IsControllerAssigned(joystick.type, joystick.id)) continue; // joystick is already assigned to a Player

            
        }

        // If all players have joysticks, enable joystick auto-assignment
        // so controllers are re-assigned correctly when a joystick is disconnected
        // and re-connected and disable this script
        if (players.Count == 0)
        {
            ReInput.configuration.autoAssignJoysticks = true;
            this.enabled = false; // disable this script
        }
    }
    /*
    // Searches all Players to find the next Player without a Joystick assigned
    private Player FindPlayerWithoutJoystick()
    {
        IList<Player> players = ReInput.players.Players;
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].controllers.joystickCount > 0) continue;
            return players[i];
        }
        return null;
    }

    private bool DoAllPlayersHaveJoysticks()
    {
        return FindPlayerWithoutJoystick() == null;
    }*/
}
