using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeniousSettings : MonoSingleton<GeniousSettings>
{
    public float pushColliderHeightOffset = 0.3f;
    public float pushColliderRadiusOffset = 0.3f;
    // public Frame comboButtonPressTiming = 0;
    public float rightStickMagnitudeCutoff = 0.5f;
    public float directionalAttackInputMagnitudeCutoff = 0.6f;
    public float pushAwaySpeed = 1f;
    public bool displaySmashSettingsDebugLogs = false;
    public float freezeFramesMultiplier = 0.38646154f;
    public int freezeFramesAdditive = 5;
    public bool debugAnimator;
    public static int fixedFrame { get; protected set; }
    [SerializeField] Transform respawnPoint;
    //public float stageSize = 35f;
    public KillZone killZone;
    public int targetFrameRate = 60;
    public float knockbackScalar = 0.08f;
    public float verticalSmashUnitScalar = 16;
    public float jumpHeightScalar = 5f;
    public float jumpSpeedScalar = 1.5f;
    public float fallspeedUnitScalar = 1;
    public bool allowVerticalMovement {get; set;} = true;
    public float horizontalSmashUnitScalar = 7;
    public float knockbackSmashUnitScalar = 0.03f;
    public float maxShield = 50;
    public float shieldDepletionPerFrame = .13f;
    public float shieldRegenerationPerFrame = 0.08f;
    public int shieldBreakFrameReductionPerButtonInput = 3;
    public int maxShieldBreakFrames = 400;
    public float shieldBreakPercentReductionMultiplier = 1f;
    public float hitStunMultiplier = 0.4f;
    public int hitStunFrameAddition = -1;
    ///<summary>
    ///When in the "side' camera view, can the character move up and down, or can they only move left and right (like smash)
    ///</summary>
    public bool disallowVerticalMovementInSideView;
    ///<summary>
    ///How many frames must a character wait before hit stun can be cancelled by an air dodge
    ///</summary>
    public Frame hitStunAirDodgeExit = 40;
    ///<summary>
    ///How many frames must a character wait before hit stun can be cancelled by an aerial
    ///</summary>
    public Frame hitStunAerialExit = 45;
    Dictionary<string, Dictionary<string, List<Move>>> moveConversions = new Dictionary<string, Dictionary<string, List<Move>>>();

    // Start is called before the first frame update
    ///<summary>
    ///How many seconds until the character will automatically get up when floored
    ///</summary>
    public float autoGetUpDuration = 2f;

    void Start()
    {
        //killZone.Grow(stageSize);
        //killZone.transform.localScale = Vector3.one * stageSize;
        Application.targetFrameRate = targetFrameRate;
        Debug("Target frame rate set to " + Application.targetFrameRate);
    }

    public Queue<string> keys = new Queue<string>();

    void RunOnSocketConnect(Action action)
    {
        StartCoroutine(RunOnSocketConnectCoroutine(action)); ;
    }

    public Vector3 GetSpawnLocation()
    {
        return respawnPoint.position;
    }

    public static void Log(string log, bool alwaysShowLog = true)
    {
        alwaysShowLog = alwaysShowLog ? alwaysShowLog : GeniousSettings.instance.displaySmashSettingsDebugLogs;
        if (alwaysShowLog) 
        {
            UnityEngine.Debug.Log(fixedFrame + ".) " + log);
        }
    }

    public static void Debug(string log)
    {
        Log(log, false);
    }

    IEnumerator RunOnSocketConnectCoroutine(Action action)
    {
        while (!GeniousSocketManager.instance.IsConnected)
        {
            yield return null;
        }

        action?.Invoke();
    }

    public void FixedUpdate()
    {
        fixedFrame++;
    }

    public Dictionary<int,  Dictionary<string, List<Move>> > moves = new Dictionary<int, Dictionary<string, List<Move>>>();
    Queue<KeyValuePair<FighterData, UnparsedMove>> moveQueue = new Queue<KeyValuePair<FighterData, UnparsedMove>>();
    public void InitializeMoveConversions(FighterData frameData)
    {
        //UnityEngine.Debug.Log("Connected? Got frame data for " + frameData.characterName);
        bool moveQueueWasEmpty = moveQueue == null || moveQueue.Count == 0;
        foreach (var upm in frameData.unparsedMoves)
        {
            moveQueue.Enqueue(new KeyValuePair<FighterData, UnparsedMove>(frameData,upm));
        }

        moves[frameData.characterId] = new Dictionary<string, List<Move>>();
        if (moveQueueWasEmpty)
        {
            GeniousSocketManager.instance.RequestMoveConversion(moveQueue.Peek().Value, frameData, HandleMoveResponse);
        }

    }

    void HandleMoveResponse(List<Move> m, FighterData frameData)
    {
        //UnityEngine.Debug.Log(frameData.characterName + " move parsed: " + m[0].ToString());
        moves[frameData.characterId][moveQueue.Dequeue().Value.InstanceId] = m;//.ToString()] = m;
        if (moveQueue.Count > 0)
        {
            GeniousSocketManager.instance.RequestMoveConversion( moveQueue.Peek().Value, moveQueue.Peek().Key, HandleMoveResponse);
        }
        else 
        {
            foreach(var pair in moves[frameData.characterId])
            {
                foreach(var move in pair.Value)
                {
                    UnityEngine.Debug.Log(frameData.characterName + " " + move.moveName + ": " + pair.Key);
                }
            }
          //  UnityEngine.Debug.Log("Setting time scale to 1f");
            Time.timeScale = 1f;
        }

        //UnityEngine.Debug.Log("Queued Jawns left: " + moveQueue.Count);
    }

    public Dictionary<string, List<Move>> GetMoveConversions(FighterData frameData)
    {
        return moves[frameData.characterId];
    }
}