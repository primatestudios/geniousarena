using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class SideViewVirtualCamera : AGeniousVirtualCamera
{
    CinemachineVirtualCamera cam;
    public CinemachineTargetGroup targetGroup;
    public Vector3 offset;
   //public Vector3 maxOffset;
    public float maxDeltaPerSecond = 50.94f;
    public float positionSmoothDamp = 0.25f;
    public float zoomSmoothDamp = 0.25f;
    public float fovOffsetPercentage = .3f;
    public float minFov;
    float zoomVelocity;
    Vector3 currentPosVelocity;


    // Update is called once per frame
    void LateUpdate()
    {
        var ogPos = transform.position;
        transform.position = targetGroup.transform.position;
        var direction = (transform.position - targetGroup.m_Targets[0].target.position);

        //90 degrees one way
        transform.rotation = Quaternion.LookRotation(Quaternion.AngleAxis(90, Vector3.up) * direction);
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z);

        transform.Translate(offset, Space.Self);
        Vector3 pos1 = transform.position;

        transform.Translate(-offset, Space.Self);

        //90 degrees the other way

        transform.rotation = Quaternion.LookRotation(Quaternion.AngleAxis(-90, Vector3.up) * direction);
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z);

        transform.Translate(offset, Space.Self);
        Vector3 pos2 = transform.position;

        Vector3 finalPos = Vector3.Distance(ogPos, pos1) < Vector3.Distance(ogPos, pos2) ? pos1 : pos2;

        transform.position = Vector3.SmoothDamp(ogPos, finalPos, ref currentPosVelocity, positionSmoothDamp);
        //transform.position = Vector3.MoveTowards(ogPos, finalPos, maxDeltaPerSecond * Time.deltaTime);

        transform.LookAt(targetGroup.transform);
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z);
        //        Debug.Log("DeltaPos: " + Vector3.Distance(ogPos, transform.position));
        //transform.localPosition += offset;

        var angle = Vector3.Angle(transform.forward, targetGroup.m_Targets[0].target.position - transform.position);
        float newZoom = Mathf.Max(minFov, angle + angle * fovOffsetPercentage);
        cam.m_Lens.FieldOfView = Mathf.SmoothDamp(cam.m_Lens.FieldOfView, newZoom, ref zoomVelocity, zoomSmoothDamp);
    }

    public void Start()
    {
        cam = GetComponent<CinemachineVirtualCamera>();
    }

    public override void OnInitialized()
    {
        if (GeniousSettings.instance.disallowVerticalMovementInSideView)
        {
            GeniousSettings.instance.allowVerticalMovement = false;
        }
    }

    public override void OnUninitialized()
    {
        GeniousSettings.instance.allowVerticalMovement = true;
    }
}