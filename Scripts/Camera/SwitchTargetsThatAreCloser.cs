using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class SwitchTargetsThatAreCloser : MonoBehaviour
{
    CinemachineBrain brain;
    CinemachineVirtualCamera vc;
    Transform target1;
    Transform target2;
    public void Start()
    {
        brain = Camera.main.GetComponent<CinemachineBrain>();
        vc = GetComponent<CinemachineVirtualCamera>();
        target1 = vc.LookAt;
        target2 = vc.Follow;
    }
    // Update is called once per frame
    void LateUpdate()
    {
        if (brain.ActiveVirtualCamera != vc)
        {
            return;
        }
        if (Vector3.Distance(transform.position, target1.position) < Vector3.Distance(transform.position, target2.position))
        {
            vc.LookAt=target2;
            vc.Follow=target1;
        }
        else
        {
            vc.LookAt = target1;
            vc.Follow = target2;
        }
    }
}
