using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitRotation : MonoBehaviour
{
    public float maxDegrees;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        while(true)
        {
            var startRotation = transform.rotation;
            yield return new WaitForEndOfFrame();
            var newRotation = transform.rotation;
            transform.rotation = Quaternion.RotateTowards(startRotation, newRotation, maxDegrees * Time.deltaTime);
        }
    }
}
