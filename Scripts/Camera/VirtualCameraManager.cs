using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualCameraManager : MonoSingleton<VirtualCameraManager>
{
    public List<CinemachineVirtualCameraBase> cameras;
    int currentIndex = -1;
    public int lastCameraIndex;
    public int preferredCameraIndex;
    
    void Start()
    {
        SetCameraIndex(0);
    }
    public void SetCameraIndex(int newIndex)
    {
        SetCurrentCameraInitialized(false);
        newIndex = newIndex < 0 || newIndex >= cameras.Count ? 0 : newIndex;
        if (newIndex != currentIndex)
        {
            lastCameraIndex = currentIndex;
            currentIndex = newIndex;
        }
        for (int i = 0; i < cameras.Count; i++)
        {
            cameras[i].Priority = i == currentIndex ? 1000 : 0;
        }

        SetCurrentCameraInitialized(true);
        Debug.Log("Set to " + cameras[currentIndex], this);
    }

    void SetCurrentCameraInitialized(bool val)
    {
        if (currentIndex >= cameras.Count || currentIndex < 0)
        {
            return;
        }
        var currentCamera = cameras[currentIndex].GetComponent<AGeniousVirtualCamera>();
        if (currentCamera != null)
        {
            if (val)
            {
                currentCamera.OnInitialized();
            }
            else
            {
                currentCamera.OnUninitialized();
            }
        }
    }
    public void LastOrPreferred()
    {
        var newIndex = currentIndex == preferredCameraIndex ? lastCameraIndex : preferredCameraIndex;
        SetCameraIndex(newIndex);
    }
    public void Next()
    {
        SetCameraIndex(currentIndex + 1);
    }
}