using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterAnimatorScript : StateMachineBehaviour
{
    protected FighterData fighterData;
    protected FighterController controlla;
    protected GameObject gameObject;
    protected Transform transform;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        fighterData = animator.gameObject.GetComponentInParent<IFighterDataHolder>().fighterData;
        controlla = animator.gameObject.GetComponentInParent<FighterController>();
        transform = animator.transform;
        gameObject = animator.gameObject;
    }
}