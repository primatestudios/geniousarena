using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleShieldFromAnimator : FighterAnimatorScript
{
    public bool toggleOnStateEnter = true;
    public bool toToggleTo;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (toggleOnStateEnter)
        {
            Toggle();
        }
    }

    // Update is called once per frame
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        if (!toggleOnStateEnter)
        {
            Toggle();
        }
    }

    void Toggle()
    {
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
           // controlla.SetShield(toToggleTo);
        });
    }
}
