using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugAnimatorState : FighterAnimatorAction
{
    public string stateName;
    [TextArea]
    public string DebugNote; //Used as like a comment, sort of
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (GeniousSettings.instance.debugAnimator)
            GeniousSettings.Debug(controlla + ".) " + stateName + "State entered");
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        if (GeniousSettings.instance.debugAnimator)
            GeniousSettings.Debug(controlla + ".) " + stateName + "State Exited");
    }
}
