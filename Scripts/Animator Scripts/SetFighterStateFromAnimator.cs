using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFighterStateFromAnimator : FighterAnimatorScript
{
    public FighterController.FighterState state;
    public bool onEnter;
    public bool onExit;
    public bool onUpdate;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (onEnter)
        {
            controlla.state = state;
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        if (onExit)
        {
            controlla.state = state;
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);
        if (onUpdate)
        {
            controlla.state = state;
        }
    }
}
