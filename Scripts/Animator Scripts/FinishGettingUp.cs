using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishGettingUp : FighterAnimatorScript 
{
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        controlla.TriggerGetUpComplete();
    }
}
