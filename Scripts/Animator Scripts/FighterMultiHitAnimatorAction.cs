using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterMultiHitAnimatorAction : FighterAnimatorAction
{
    public List<Move> multiHitMoves = new List<Move>();
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //unparsedMove = moves[0];//For now
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }
}