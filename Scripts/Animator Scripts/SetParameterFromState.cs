using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SetParameterFromState : StateMachineBehaviour
{
    public string parameterName;
    public bool setOnEnter;
    public bool setOnExit;
    public bool setOnUpdate;
    protected int? paramHash;
    protected Animator a;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, animatorStateInfo, layerIndex);
        if (!paramHash.HasValue)
        {
            paramHash = Animator.StringToHash(parameterName);
        }

        a = animator;
        if (setOnEnter)
        {
            Set();
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        if (setOnExit)
        {
            Set();
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);
        if (setOnUpdate)
        {
            Set();
        }
    }

    protected abstract void Set();
}
