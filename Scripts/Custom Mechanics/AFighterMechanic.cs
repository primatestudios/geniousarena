using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AFighterMechanic : MonoBehaviour
{
    FighterController controlla;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        controlla = GetComponentInParent<FighterController>();
    }
}
