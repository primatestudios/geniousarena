using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPoint : APlayerControllerAccessory
{
    Vector3? _position;
    public Vector3 position
    {
        get
        {
            if (_position.HasValue)
            {
                return _position.Value;
            }

            return transform.position;
        }

        set
        {
            _position = value;
        }
    }

    public void ResetPosition()
    {
        _position = null;
    }
}
