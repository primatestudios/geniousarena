using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FighterPercentUI : MonoBehaviour
{
    public float percentSignScale = 0.4f;
    public TextMeshProUGUI percentText;
    public TextMeshProUGUI nameText;
    public Color finalColor;
    public Color middleColor;
    public float finalColorPercent;
    public Image portrait;
    public FighterController referenceCharacter;
    [Range(0f, 1f)]
    public float screenHeightPercentage;
    Color startColor;
    public IEnumerator Start()
    {
        percentText.enableAutoSizing = false;
        startColor = percentText.color;
        ((RectTransform)transform).sizeDelta = Vector2.one * Screen.height * screenHeightPercentage;
        /*GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            percentText.enableAutoSizing = false;
        });*/
        yield return new WaitForEndOfFrame();
        UpdateValues();

        //portrait.sprite = referenceCharacter.frameData.portrait;
    }
    float lastPercent = -123455f;
    public void Update()
    {
        if (referenceCharacter != null)
        {
            if (!referenceCharacter.gameObject.activeSelf && referenceCharacter.otherChar != null)
            {
                referenceCharacter = referenceCharacter.otherChar;
                UpdateValues();
            }
            else if (lastPercent != referenceCharacter.percent)
            {
                UpdateValues();
            }
        }

        lastPercent = referenceCharacter.percent;
    }

    void UpdateValues()
    {
        nameText.text = referenceCharacter.fighterData.displayName;
        portrait.sprite = referenceCharacter.fighterData.portrait;// characterPortrait;
        percentText.text = (Mathf.RoundToInt(referenceCharacter.percent)).ToString() + "<size=" + (percentText.fontSize * percentSignScale) + ">%</size>";
        Color c;
        if (referenceCharacter.percent > finalColorPercent / 2f)
        {
            c = Color.Lerp(middleColor, finalColor, finalColorPercent / 2 / finalColorPercent);
        }
        else
        {
            c = Color.Lerp(startColor, middleColor, referenceCharacter.percent / (finalColorPercent / 2f));
        }
        percentText.color = c;
    }
}
