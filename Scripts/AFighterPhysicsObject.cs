using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Input.Plugins.PlayerInput;

public abstract class AFighterPhysicsObject : MonoBehaviour, IFighterDataHolder
{
    [SerializeField] protected Animator m_Animator { get; set; } // Reference used to make decisions based on Ellen's current animation and to set parameters.

    protected readonly int m_HitStunned = Animator.StringToHash("HitStunned");
    protected readonly int m_HashHurt = Animator.StringToHash("Hurt");
    protected readonly int m_DamagedFall = Animator.StringToHash("DamagedFall");
    protected readonly int m_Floored = Animator.StringToHash("Floored");
    /*  public bool damaged {
          get {
              return m_Animator.GetBool(m_DamagedFall);
          }
          set{
              m_Animator.SetBool(m_DamagedFall, value);
          }
      }*/
    public virtual bool isInGuard { get { return false; } }
    protected bool invulnerable;

    public bool floored { get; set;
        /*
        get {
            return m_Animator.GetBool(m_Floored);
        }
        set{
            m_Animator.SetBool(m_Floored, value);
        }*/
    }
    public int lastHitFrame;
    bool damagedFall
    {
        get
        {
            return m_Animator.GetBool(m_DamagedFall);
        }
        set
        {
            m_Animator.SetBool(m_DamagedFall, value);
        }
    }
    public bool hitStunned
    {
        get
        {
            return m_Animator.GetBool(m_HitStunned);
        }
        private set
        {
            m_Animator.SetBool(m_HitStunned, value);
        }
    }
    public bool hitStunAerialCancellable { get; private set; }// = true;
    public bool hitStunAirDodgeCancellable { get; private set; }// = true;

    public FighterData fighterData { get { return _fighterData; } protected set { _fighterData = value; } }
    [SerializeField] FighterData _fighterData;
    public float percent;
    public FighterShield shield;
    public bool m_IsGrounded = true;// { get; set; }
    Coroutine knockbackCoroutine;
    public virtual void HandleKnockbackResponse(KnockBackResponse knockback, AFighterPhysicsObject damageSource, Move attack)
    {
        Debug.Log("Ping: " + (Time.time - FighterController.lastRequestedTime));
        percent += knockback.damage;
        ResolveFreezeFrames(knockback.damage);

        Debug.Log(GeniousSettings.fixedFrame + ".) Hit with " + attack.moveName + " for " + knockback.damage + "% (Currently " + percent + "\n" + knockback);

        if (knockbackCoroutine != null)
        {
            StopCoroutine(knockbackCoroutine);
        }
        knockbackCoroutine = StartCoroutine(Knockback(knockback, damageSource));
    }

    public Vector2 diStick;
    Coroutine hitStunCancelCoroutine;
    protected virtual IEnumerator Knockback(KnockBackResponse knockback, AFighterPhysicsObject damageSource)
    {
        if (invulnerable)
        {
            yield break;
        }
        floored = false;
        var startPosition = transform.position;
        var direction = transform.position - damageSource.transform.position;
        var angle = Vector3.SignedAngle(Vector3.right, direction, Vector3.up);
        if (isInGuard)
        {

            AdjustShieldHealth(-knockback.shield_damage);
            yield break;
        }
        Vector3 ogDamageSource = damageSource.transform.position;
        if (hitStunCancelCoroutine != null)
        {
            StopCoroutine(hitStunCancelCoroutine);
        }
        hitStunCancelCoroutine = StartCoroutine(TrackHitStun(knockback.hitstun, knockback.airdodge_hitstun_cancel, knockback.aerial_hitstun_cancel));

        if (knockback.reeling)
        {
            TriggerReeling();
        }
        else if (knockback.tumble)
        {
            TriggerTumbling();
        }

        damagedFall = knockback.reeling || knockback.tumble;
        LaunchTrajectory lastTrajectory = new LaunchTrajectory { position = new Position()};// lastTraj
        foreach (var trajectory in knockback.launch_trajectory)
        {
            var launchSpeed = Mathf.Sqrt(Mathf.Pow((float) knockback.horizontal_launch_speed, 2) + Mathf.Pow((float)knockback.vertical_launch_speed, 2)); //Include gravity boost to the new launch speed (yes this only happens when stick isn't on neutral)
            var scaledStick = diStick * 127;
            var diAngle = DI(scaledStick, new Vector2((float) knockback.horizontal_launch_speed, (float)knockback.vertical_launch_speed), launchSpeed, .17f);
            var distance = Mathf.Sqrt(Mathf.Pow(trajectory.vector.x, 2) + Mathf.Pow(trajectory.vector.y, 2));//Vector2.Distance(trajectory.vector, Vector2.zero);
            var angleToAdjust = diAngle - knockback.launch_angle;

            var a = Mathf.Atan2(trajectory.position.y, trajectory.position.x) * Mathf.Rad2Deg;
            var newAngle = Mathf.Deg2Rad * (a + angleToAdjust);
            if (distance != 0 && !float.IsNaN(distance))
            {
                var oldTrajectory = trajectory.vector;
                trajectory.vector = new Vector2(Mathf.Cos(newAngle) * distance, Mathf.Sin(newAngle) * distance);
             //   Debug.Log((Mathf.Rad2Deg * newAngle) + " from " + a + " + " +  angleToAdjust + " and " + distance + " -> " + trajectory.vector + " from " + oldTrajectory);
            }
            /*REVISION  7e6744b60bef01ab2aee8dae32257d39b3cdb6c9
                        var a = Mathf.Atan2(trajectory.position.y - lastTrajectory.position.y, trajectory.position.x - lastTrajectory.position.x) * Mathf.Rad2Deg;

                        var distance = Vector2.Distance(trajectory.vector, lastTrajectory.vector);
                        var newAngle = a + angleToAdjust;
                      //  Debug.Log(trajectory.vector + " before - " + distance + "(new Angle: " + newAngle + " from " + diAngle + " - " + knockback.launch_angle + " and " + a);
                        if (distance != 0)
                        {
                       //     trajectory.vector = new Vector2(Mathf.Cos(newAngle) * distance, Mathf.Sin(newAngle) * distance);
                        }
                       // Debug.Log(trajectory.vector + " after"); 
            */
            var startRotation = transform.rotation;
            Vector3 flatDirection = -transform.forward;
            //flatDirection.x = 90; 
            transform.LookAt(ogDamageSource);
            var translation = -transform.forward * trajectory.position.x;
            translation.y = trajectory.position.y;
            Vector3 lastPosition = transform.position;

            MoveDirect(startPosition + translation * GeniousSettings.instance.knockbackScalar - transform.position);
            Vector3 newDirection = transform.position - lastPosition;
            transform.rotation = startRotation;
            yield return new WaitForFixedUpdate();
            lastTrajectory = trajectory;
        }

        StartCoroutine(HandleAfterLandingCompleted());
    }

    IEnumerator HandleAfterLandingCompleted()
    {
        while (!m_IsGrounded)
        {
            yield return new WaitForFixedUpdate();
        }

        if (damagedFall)
        {
            SetFloored();
            damagedFall = false;
        }
        else
        {
            SetNormalState();
        }

        if (hitStunned)
        {
            CancelHitStun();
        }
    }

    protected virtual void AdjustShieldHealth(float damageAmount)
    {
        shield.AdjustShieldHealth(damageAmount);
    }

    IEnumerator TrackHitStun(int hitStunLength, int hitStunAirDodgeCancelLength, int hitStunAerialCancelLength)
    {
        int startFrame = GeniousSettings.fixedFrame;
        while (hitStunLength-- > 0)
        {
            if (hitStunned == false)
            {
                yield break;
            }
            hitStunAerialCancellable = startFrame + hitStunAerialCancelLength <= GeniousSettings.fixedFrame;
            hitStunAirDodgeCancellable = startFrame + hitStunAirDodgeCancelLength <= GeniousSettings.fixedFrame;

            yield return new WaitForFixedUpdate();
            
        }
        CancelHitStun(true);
    }

    public virtual void TriggerTumbling()
    {

    }

    public virtual void TriggerReeling()
    {

    }

    protected virtual void SetFloored()
    {
        Debug.Log("Where did this get set?");
        floored = true;
        GenericCoroutineManager.instance.RunInSeconds(GeniousSettings.instance.autoGetUpDuration, () =>
        {
            TriggerGetUp();
        });
    }

    protected virtual void SetNormalState()
    {

    }

    public virtual void TriggerTumbling(int hitStunLength)
    {

    }

    public virtual void TriggerGetUp()
    {
        m_Animator.SetBool("GetUp", true);
        GenericCoroutineManager.instance.runOnNextUpdate += () => m_Animator.SetBool("GetUp", false);
    }

    public virtual void GettingUp()
    {
        floored = false;
    }

    protected void CancelHitStun(bool allowFlooredLanding = false)
    {
        Debug.Log(GeniousSettings.fixedFrame + ".) Cancelling hit stun" );
        if (!allowFlooredLanding)
        {
            damagedFall = false;
        }
        hitStunned = false;
        //damaged = false;
    }

    public abstract void MoveDirect(Vector3 motion);

    public void SetHurtTrigger()
    {
        m_Animator.SetBool("New Bool", true);
        GenericCoroutineManager.instance.RunInFixedUpdateFrames(2, () =>
        {
            m_Animator.SetBool("New Bool", false);
        });
        m_Animator.SetTrigger(m_HashHurt);
        GenericCoroutineManager.instance.RunInFrames(1, () => {
            m_Animator.ResetTrigger(m_HashHurt);
        });
    }
    public virtual void RegisterHurt(HitPoint hitPoint, Move move)
    {
        SetHurtTrigger();
        Debug.Log("Move: " + move);
        
        lastHitFrame = GeniousSettings.fixedFrame;
        hitStunned = true;
        GeniousEffectManager.instance.GenerateHitEffect(hitPoint.position);
        ResolveFreezeFrames(move.base_damage);

    }

    void ResolveFreezeFrames(float damage)
    {
        Frame numFrames = GetFreezeFrameValue(damage);
        if (freezeFramesCoroutine != null)
        {
            float secondsCompletedSoFar = Time.realtimeSinceStartup - freezeFrameStarted.Value;
            StopCoroutine(freezeFramesCoroutine);
            if (numFrames.seconds <= secondsCompletedSoFar)
            {
                FinishFreezeFrames();
                return;
            }
            else
            {
                numFrames = numFrames.value - Mathf.RoundToInt(secondsCompletedSoFar * GeniousSettings.instance.targetFrameRate);
            }
        }

        FreezeFrames(numFrames);
    }

    Coroutine freezeFramesCoroutine;
    float? freezeFrameStarted;
    void FreezeFrames(Frame numFrames)
    {
        Time.timeScale = 0f;
        freezeFrameStarted = Time.realtimeSinceStartup;
        freezeFramesCoroutine = GenericCoroutineManager.instance.RunInSecondsRealtime(numFrames.seconds, () =>
        {
            FinishFreezeFrames();
        });
    }

    void FinishFreezeFrames()
    {
        Time.timeScale = 1f;
        freezeFramesCoroutine = null;
        freezeFrameStarted = null;
    }

    Frame GetFreezeFrameValue(float damage)
    {
        var modifierMultipler = 1f;
        Frame numFramesToWait = Mathf.FloorToInt((damage * GeniousSettings.instance.freezeFramesMultiplier + GeniousSettings.instance.freezeFramesAdditive) * modifierMultipler);
        return numFramesToWait;
        // return numFramesToWait.seconds;
    }

    protected virtual void FixedUpdate()
    {
    }

    float StickSensibility(float value)
    {
        if (value < 24 && value > -24)
            return 0;
        if (value > 118)
            return 1;
        if (value < -118)
            return -1;
        return value / 118;
    }

    float DI(Vector2 stick, Vector2 launchSpeed, float totalLaunchSpeed, float di)
    {
        if (totalLaunchSpeed < 0.00001f) //There is an if on MSC but it shouldn't happen since it requires tumble for DI to work
            return Mathf.Atan2(launchSpeed.y, launchSpeed.x) * 180 / Mathf.PI;

        if (Mathf.Abs(Mathf.Atan2(launchSpeed.y, launchSpeed.x)) < di) //Cannot DI if launch angle is less than DI angle change param
            return Mathf.Atan2(launchSpeed.y, launchSpeed.x) * 180 / Mathf.PI;

        var X = StickSensibility(stick.x);
        var Y = StickSensibility(stick.y);

        var check = Y * launchSpeed.x - X * launchSpeed.y < 0;

        var variation = Mathf.Abs(X * launchSpeed.y - Y * launchSpeed.x) / totalLaunchSpeed;

        di = di * variation;

        var angle = 0;

        if (check)
            angle = Mathf.RoundToInt((Mathf.Atan2(launchSpeed.y, launchSpeed.x) - di) * 180 / Mathf.PI);
        else
            angle = Mathf.RoundToInt((Mathf.Atan2(launchSpeed.y, launchSpeed.x) + di) * 180 / Mathf.PI);

        if (angle < 0)
            angle += 360;

        return angle;
    }
}