using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterShield : APlayerControllerAccessory
{
    Vector3 maxShieldScale;
    public float shieldHealth;
    public Action onShieldBreak;
    // Start is called before the first frame update
    void Awake()
    {
        maxShieldScale = transform.localScale;
        shieldHealth = GeniousSettings.instance.maxShield;
        SetActive(false);
    }

    public void SetActive(bool val)
    {
        gameObject.SetActive(val);
    }

    public void AdjustShieldHealth(float damageAmount)
    {
        shieldHealth += damageAmount;
        shieldHealth = Mathf.Min(GeniousSettings.instance.maxShield, Mathf.Max(0, shieldHealth));
        float scale = shieldHealth / GeniousSettings.instance.maxShield;
        transform.localScale = maxShieldScale * scale;
        if (shieldHealth <= 0)
        {
            onShieldBreak?.Invoke();
        }
    }
}
