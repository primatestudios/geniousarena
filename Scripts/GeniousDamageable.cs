using Gamekit3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeniousDamageable : Damageable
{
    public AFighterPhysicsObject physicsObject;
    public float damagePercent { get; protected set; }

    public void AddDamage(float percent)
    {
        if (!isInvulnerable)
        {
            damagePercent += percent;
            m_timeSinceLastHit = 0f;
            isInvulnerable = true;
        }
    }

    public override void ApplyDamage(DamageMessage data)
    {
        base.ApplyDamage(data);
        physicsObject.SetHurtTrigger();
    }


}
