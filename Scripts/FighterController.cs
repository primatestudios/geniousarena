using Gamekit3D;
using Rewired;
using Smash_Forge;
using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Experimental.Input;
using UnityEngine.Serialization;
using static Smash_Forge.ATKD;

public class MoveConversions
{
    public Dictionary<string, List<MoveConversions>> moveConversion;
}

[RequireComponent(typeof(CapsuleCollider))]
public class FighterController : PlayerController,  IHitboxActivator
{
    FighterModel model;
    public bool rightStickSmash = true;
    [SerializeField] CapsuleCollider personalSpace;
    public override bool isInGuard { get { return state == FighterState.Guard; } }
    public int ignoreRootMotionOnThisFrame;
    public Transform landingDirectionTransform;
    public FighterPlayerInput smashInput;
    //[SerializeField] SmashFrameData frameData;
    [Range(0f, 1f)]
    [SerializeField]
    float inputDeadZone = 0.1f;
    readonly int m_FacingUp = Animator.StringToHash("FacingUp");
    readonly int m_HorizontalInput = Animator.StringToHash("HorizontalInput");
    readonly int m_VerticalInput = Animator.StringToHash("VerticalInput");
    readonly int m_InputMagnitude = Animator.StringToHash("InputMagnitude");

    readonly int m_AttackDetected = Animator.StringToHash("AttackDetected");
    readonly int m_AttackStarted = Animator.StringToHash("AttackStarted");

    readonly int m_UpSpecial = Animator.StringToHash("UpSpecial");
    readonly int m_DownSpecial = Animator.StringToHash("DownSpecial");
    readonly int m_ForwardSpecial = Animator.StringToHash("ForwardSpecial");
    readonly int m_DownSmash = Animator.StringToHash("DownSmash");
    readonly int m_UpSmash = Animator.StringToHash("UpSmash");
    readonly int m_ForwardSmash = Animator.StringToHash("ForwardSmash");
    readonly int m_UpTilt = Animator.StringToHash("UpTilt");
    readonly int m_DownTilt = Animator.StringToHash("DownTilt");



    readonly int m_AirDodged = Animator.StringToHash("DodgedInAir");
    readonly int m_JumpSquatTriggered = Animator.StringToHash("JumpSquatTriggered");

    readonly int m_JumpsLeft = Animator.StringToHash("HopsLeft");
    readonly int m_ExitLocomotion = Animator.StringToHash("ExitLocomotion");

    const string m_moveableAnimatorTag = "Moveable";

    Action onNextAnimatorFrame;
    public bool shieldButtonPressed;
    //public float shieldHealth;
    //[SerializeField] float knockbackScalar = 0.4f;
    // [SerializeField] float airMovementScalar;
    [SerializeField] float rollScalar;
    HashSet<InputAction> attackDetectedActions = new HashSet<InputAction>();
    Dictionary<FighterState, int> smashPlayerStateAnimatorReferences = new Dictionary<FighterState, int>();
    //Normal state can not be named "Grounded" because another animator parameter uses it. 
    //None of these states can be other animtor parm names
    //TODO - prefix each state in the animator with "SmashState_"
    public enum FighterState { Normal, Tumbling, Reeling, Helpless, Stunned, Sleeping, Frozen, Buried, Floored, GettingUp, Guard, Dodging, Dead }
    [SerializeField] FighterState _state;
    public FighterState state {
        get { return _state; }
        set
        {
            if (value == _state)
            {
                return;
            }
            GeniousSettings.Debug("Setting state to " + value);
            foreach (var s in smashPlayerStateAnimatorReferences)
            {
                m_Animator.SetBool(s.Value, value == s.Key);
            }
            if (onStateCallbacks.ContainsKey(state))
            {
                onStateCallbacks[state]?.Invoke(false);
            }

            _state = value;
            if (onStateCallbacks.ContainsKey(state))
            {
                onStateCallbacks[state]?.Invoke(true);
            }
        }
    }

    Dictionary<FighterState, Action<bool>> onStateCallbacks = new Dictionary<FighterState, Action<bool>>();
    public void RegisterStateCallback(FighterState callbackState, Action<bool> action)
    {
        if (onStateCallbacks.ContainsKey(callbackState))
        {
            onStateCallbacks[callbackState] += action;
        }
        else
        {
            onStateCallbacks[callbackState] = action;
        }
    }

    public Action<FighterState> OnState;

    float airMovementScalar
    {
        get
        {
            return (float)fighterData.attributes.air_speed * GeniousSettings.instance.horizontalSmashUnitScalar / maxForwardSpeed;
        }
    }

    protected override bool m_InCombo
    {
        get
        {
            return false;
        }

        set
        {
            base.m_InCombo = value;
        }
    }

    public override float jumpSpeed
    {
        get
        {
            Frame fullHop = fighterData.attributes.fh_air_time;
            var height = shortHop ? fighterData.attributes.hop_height : fighterData.attributes.jump_height;
            return (float)height * GeniousSettings.instance.verticalSmashUnitScalar;// fullHop.seconds / 2f * SmashSettings.instance.verticalSmashUnitScalar;
        }
    }

    public override float maxForwardSpeed
    {
        get
        {
            return (float)(isMovingForward ? dashSpeed : fighterData.attributes.walk_speed * GeniousSettings.instance.horizontalSmashUnitScalar);
        }
    }

    public float dashSpeed
    {
        get {
            return (float)fighterData.attributes.run_speed * GeniousSettings.instance.horizontalSmashUnitScalar;
        }
    }

    public override float gravity
    {
        get
        {
            if (fighterData.attributes.fall_speed == 0)
            {
                Debug.LogError("Error: Frame data isn't properly instantiated");
            }
            return (float)fighterData.attributes.fall_speed * GeniousSettings.instance.verticalSmashUnitScalar;
        }
    }

    public float minFallSpeed { get { return Convert.ToSingle(-fighterData.attributes.fall_speed) * GeniousSettings.instance.fallspeedUnitScalar; } }

    protected override bool Jab
    {
        get
        {
            return smashInput.GetButtonValue(RewiredInputActions.instance.Jab);
        }
    }

    protected override bool JumpInput
    {
        get
        {
            return smashInput.GetButtonValue(RewiredInputActions.instance.Jump);
        }
    }

    protected override bool IsMoveInput
    {
        get { return base.IsMoveInput || isMovingForward; }
    }

    int activatedFrame = -1;
    bool lastVal;
    public Action onHitAnimatorCallback;

    public void SetOnHitCallback(Action onHit)
    {
        onHitAnimatorCallback = onHit;
    }

    int? currentStateHash;
    int? hitDetectedHash;
    public void SetCurrentStateHash(int hash)
    {
        currentStateHash = hash;
    }

    int frameId;
    public void ActivateHitbox(Frame frames, Move move)
    {
        lastMoveId = move.api_id;
        hitbox.size = new Vector3(hitbox.size.x, move.hitbox.rect.height, move.hitbox.rect.width);
        //Debug.Log(hitboxYOffset + " + (" + move.hitbox.rect.yMax + "*" + transform.localScale.y);
        hitbox.center = new Vector3(hitbox.center.x, hitboxYOffset + (move.hitbox.rect.yMin * transform.localScale.y), move.hitbox.rect.center.x);
        //hitbox.center = new Vector3(hitbox.center.x, (move.hitbox.rect.yMin * transform.localScale.y), move.hitbox.rect.center.x);

        foreach (var trail in weaponTrails)
        {
            trail.Emit = true;
        }

        SetHitboxActive(true);
        frameId = Time.frameCount;
        int localFrameId = frameId;
        GenericCoroutineManager.instance.RunInFixedUpdateFrames(frames.value + 1, () =>
        {
            if (frameId == localFrameId)
            {
                SetHitboxActive(false);
            }
        });
    }

    void SetTrigger(int hash, bool setAttackDetected = false)
    {
        if (setAttackDetected)
        {
            attackDetected = true;
        }
        m_Animator.SetTrigger(hash);
        onNextAnimatorFrame += () => { m_Animator.ResetTrigger(hash); };
    }

    public void SetHitboxActive(bool val)
    {
        if (lastVal != val)
        {
            if (val)
            {
                activatedFrame = GeniousSettings.fixedFrame;
                // Debug.Log("Hitbox activated on frame " + (activatedFrame - frameMoveStarted));
            }
            else
            {
                if (Time.frameCount - activatedFrame == 0)
                {
                    return;
                }

                if (activatedFrame > -1)
                {
                    Frame f = (GeniousSettings.fixedFrame - activatedFrame);

                    //                    Debug.Log("Hitbox activated for " + f + " frames - (probably " + (f.seconds) + " seconds)");
                }
            }
        }

        lastVal = val;
        /*foreach (var hitbox in meleeWeapon.attackPoints)
        {
            hitbox.attackRoot.gameObject.SetActive(val);
        }*/
        if (hitbox != null)
        {
            hitbox.SetActive(val);
        }
    }

    public void UpdateLandingDirection()
    {
        var ogPosition = landingDirectionTransform.position;
        landingDirectionTransform.Translate(Vector3.right, Space.Self);
        m_Animator.SetBool(m_FacingUp, landingDirectionTransform.position.y >= ogPosition.y);
        landingDirectionTransform.position = ogPosition;
    }   

    int frameMoveStarted;
    //Dictionary<string, List<Move>> moveConversion { get { return GeniousSettings.instance.GetMoveConversions(fighterData); } }

    string _currentMoveId;
    public string lastMoveId { get {
            return _currentMoveId;
        } set {
            _currentMoveId = value;
            string moveName = fighterData.GetMove(_currentMoveId)?.name;
            if (!String.IsNullOrWhiteSpace(moveName) && moveName.Contains("Roll"))
            {
                TriggerRoll();
            }
        }
    }
    public HitBox hitbox;
    float hitboxYOffset;
 

    Transform pushAwayFrom;
    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject != gameObject && other.gameObject.HasComponent<AFighterPhysicsObject>())
        {
            pushAwayFrom = other.transform;
        }
    }

    public void MoveComplete(string moveId)
    {
        if (moveId == lastMoveId)
        {
            foreach (var trail in weaponTrails)
            {
                trail.Emit = false;
            }
        }

        currentStateHash = null;
        hitDetectedHash = null;
    }

    protected override Vector2 MoveInput
    {
        get
        {
            if (isMovingForward)
            {
                return Vector2.right;
            }

            Vector2 ret = smashInput.GetVector2(RewiredInputActions.instance.MovementHorizontal, RewiredInputActions.instance.MovementVertical);
            if (!GeniousSettings.instance.allowVerticalMovement)
            {
                ret.y = 0f;
            }
            return ret;
        }
    }

    public void TriggerRoll()
    {
        state = FighterState.Dodging;
        StartCoroutine(Roll());
    }

    public void TriggerAirDodge()
    {
        m_Animator.SetBool(m_AirDodged, true);
    }

    //http://kuroganehammer.com/Smash4/Formulas
    Frame CalculateHitStun(GeniousDamageable target, AttackFrameData attack, int baseDamage)
    {
        return Mathf.FloorToInt(CalculateKnockback(target, attack, baseDamage) * 0.4f) - 1;
    }

    float CalculateKnockback(GeniousDamageable target, AttackFrameData attack, int hitIndex)
    {
        var v = target.damagePercent;
        var bd = GetWantedHitboxValue(attack.baseDamage, hitIndex);
        var w = 0f;//target.smashFrameData.weight;
        var g = GetWantedHitboxValue(attack.knockbackGrowth, hitIndex);
        var b = GetWantedHitboxValue(attack.baseKnockBack, hitIndex);
        var s = 8f;

        /*Stale Move Multiplier:
         *  1-Σ(sn/100)
         *  S1=8.000
            S2=7.594
            S3=6.782
            S4=6.028
            S5=5.274
            S6=4.462
            S7=3.766
            S8=2.954
            S9=2.200
         * */
        float r = 1f;
        /*r = Ratio where:
        Rage Multiplier = Variable
        Crouch Cancel = 0.85
        Grounded Meteor = 0.8
        Charging Smash = 1.2
        */
        var knockback = ((((((v + bd * s) / 10 + (((v + bd * s) * bd * (1 - (1 - s) * 0.3)) / 20)) * 1.4 * (200 / (w + 100))) + 18) * (g / 100)) + b) * r;
        return (float)knockback;
    }

    T GetWantedHitboxValue<T>(List<T> list, int hitIndex)
    {
        var index = list.Count - hitIndex - 1;
        if (index < 0)
        {
            index = 0;
        }

        return list[index];
    }

    public void AttackCalculator(GeniousDamageable d, HitBox hitbox, HitPoint hitPoint)
    {
        /*for (int i = 0; i < meleeWeapon.attackPoints.Length; i++)
        {
            int endIndex = meleeWeapon.attackPoints.Length - 1 - i;
            if (meleeWeapon.attackPoints[endIndex].attackRoot == hitbox.transform)
            {
                break;
            }
        }
        */
        //Already registered a hit during this state
        if (hitDetectedHash.HasValue && hitDetectedHash == currentStateHash) 
        {
            return;
        }
        Debug.Log("CurrentMoveId: " + lastMoveId );

        Move move = fighterData.GetMove(lastMoveId);
        Debug.Log("CurrentMoveId: " + lastMoveId + " - " + move?.moveName);

        //if (moveConversion.ContainsKey(currentMoveId))
        if (move != null)
        {
            Debug.Log("Move Name: " + move.name);
            lastRequestedTime = Time.time;
            d.physicsObject.RegisterHurt(hitPoint, move);
            onHitAnimatorCallback?.Invoke();
            move.chargedFrames = 0;//TODO - up this as needed
            GeniousSocketManager.instance.SendKnockbackRequest(new KnockbackRequest(move,move.chargedFrames, this, (FighterController) d.physicsObject));
        }
        else
        {
            Debug.LogError("Can't find the conversion? " + lastMoveId);
        }
    }

   /* List<Move> GetParsedMove(UnparsedMove upm)
    {
        if (moveConversion.ContainsKey(upm.InstanceId))
        {
            return moveConversion[upm.InstanceId];
        }
        return null;
    }*/

    public static float lastRequestedTime;
    public void Attack(GeniousDamageable d, HitBox hitbox, HitPoint hitPoint)
    {
        AttackCalculator(d, hitbox, hitPoint);
        /*if (currentAction is AttackFrameData && !d.isInvulnerable)
        {
            var attack = (AttackFrameData)currentAction;
            if (attack.baseDamage == null || attack.baseDamage.Count == 0)
            {
                Debug.LogError("Cannot apply damage because " + attack.actionName + " base damage is empty");
                return;
            }

            Debug.Log("Base damage maybe should be" + attack.baseDamage[0]);
            float knockback = 0f; //CalculateKnockback(d, attack, i);
            int baseDamage = 0;//GetWantedHitboxValue(attack.baseDamage, i);
            int hitAngle = 0;

            Debug.Log(hitbox.transform, hitbox);
            for (int i = 0; i < meleeWeapon.attackPoints.Length; i++)
            {
                int endIndex = meleeWeapon.attackPoints.Length - 1 - i;
                if (meleeWeapon.attackPoints[endIndex].attackRoot == hitbox.transform)
                {
                    knockback = CalculateKnockback(d, attack, i);
                    baseDamage = GetWantedHitboxValue(attack.baseDamage, i);
                    hitAngle = GetWantedHitboxValue(attack.angle, i);
                    d.invulnerabiltyTime = CalculateHitStun(d, attack, i).seconds; //TODO - this isn't what hitstun is, probably
                    break;
                }
                else
                {
                    Debug.Log(endIndex + " " + meleeWeapon.attackPoints[endIndex].attackRoot + " != " + hitbox, meleeWeapon.attackPoints[endIndex].attackRoot);
                }
            }

            d.AddDamage(baseDamage);
        }
        */
    }

    public Move GetCurrentMove(int index = -1)
    {
        return fighterData.GetMove(lastMoveId, index);
    }

    DodgeFrameData forwardRoll;
    IEnumerator Roll()
    {
        var m = GetCurrentMove();
        string[] split = m.unparsedMove.HitboxActive.Split('-');
        int firstFrame = Convert.ToInt32(split[0].Trim());
        int lastFrame = Convert.ToInt32(split[1].Trim());

        for (int i = 0; i < Convert.ToInt32(m.faf) - 1; i++)
        {
            invulnerable = i >= firstFrame && i <= lastFrame;
            yield return new WaitForEndOfFrame();
        }

        SetNormalState();

        if (smashInput.GetButtonValue(RewiredInputActions.instance.Block))
        {
            SetShield(true);
        }
    }

    Dictionary<InputAction, float> activeButtons = new Dictionary<InputAction, float>();
    Dictionary<InputAction, int> inputTimeHashKeys = new Dictionary<InputAction, int>();

    protected override bool InputDetected
    {
        get
        {
            return base.InputDetected || activeButtons.Count > 0;
        }
    }

    bool canRotate = true;
    public void SetRotationAllowed(bool val)
    {
        canRotate = val;
    }

    public void SetMovementAllowed(bool val, bool updateParentTransformHere = false)
    {
        canMove = val;
    }

    Action nextAnimatorFrame;
    protected override void OnAnimatorMove()
    {
        if (animationMovementOffset.y <= 0)
        {
            base.OnAnimatorMove();
        }
        if (nextAnimatorFrame != null)
        {
            nextAnimatorFrame.Invoke();
        }
        nextAnimatorFrame = onNextAnimatorFrame;
        onNextAnimatorFrame = null;
    }

    protected override void HandleMeleeAttack()
    {
        //Don't do anything here. Handle it how you handle the other attacks
    }

    public override void Kill()
    {
        if (state == FighterState.Dead)
        {
            return;
        }

        state = FighterState.Dead;
        if (m_VerticalSpeed > 0)
        {
            m_VerticalSpeed = minFallSpeed;
        }
        GeniousEffectManager.instance.GenerateDeathEffect(transform.position, () => {
            percent = 0f;
            transform.position = GeniousSettings.instance.GetSpawnLocation();
            SetNormalState();
            StopAllCoroutines();
            m_Animator.Rebind();
        });
    }

    public void SetShield(bool val)
    {
        shieldButtonPressed = val;
        FighterState toSet = state;
        if (val && !CheckRoll() && state == FighterState.Normal && m_IsGrounded)
        {
            toSet = FighterState.Guard;
        }
        else if (!val && state == FighterState.Guard)//Guard dropped
        {
            toSet = FighterState.Normal;
        }

       // shield.SetActive(val);
        state = toSet;
    }

    bool isMovingForward;

    void OnMoveForward(bool val)
    {
        isMovingForward = val;
    }

    void ToggleCamera(bool val)
    {
        if (val)
        {
            VirtualCameraManager.instance.Next();
        }
    }

    bool shortHop = false;
    int? jumpInputFrame;
    int jumpFrame;
    bool squatting = false;
    void OnJump(bool val)
    {
        //Debug.Log("OnJump!");
        if (state != FighterState.Normal)
        {
            return;
        }
        if (m_IsGrounded && val && !jumpInputFrame.HasValue)
        {
            squatting = true;
            m_Animator.SetTrigger(m_JumpSquatTriggered);
            jumpInputFrame = GeniousSettings.fixedFrame;
            GenericCoroutineManager.instance.RunInFixedUpdateFrames(fighterData.attributes.jumpsquat, () =>
            {
                m_Animator.ResetTrigger(m_JumpSquatTriggered);
                squatting = false;
                shortHop = !jumpInputFrame.HasValue;
                TriggerJump(true);
            });
        }

        if (!val)
        {
            jumpInputFrame = null;
        }
    }

    bool jumpTriggered;
    public void TriggerJump(bool bounce = false)
    {
        jumpTriggered = true;
        jumpFrame = GeniousSettings.fixedFrame;
        if (bounce)
        {
            //   m_VerticalSpeed = jumpSpeed;
        }
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            m_Animator.SetInteger(m_JumpsLeft, m_Animator.GetInteger(m_JumpsLeft) - 1);
        });
    }

    protected override void CalculateVerticalMovement()
    {
        m_IsGrounded = m_CharCtrl.isGrounded;

        if (animationMovementOffset.y > 0)
        {
            m_VerticalSpeed = 0f;
            return;
        }
        if (m_IsGrounded && !jumpTriggered)
        {
            // When grounded we apply a slight negative vertical speed to make Ellen "stick" to the ground.
            m_VerticalSpeed = -gravity * k_StickingGravityProportion;
        }
        else if (jumpTriggered)
        {
            SetVerticalSpeed(true);
        }
        else
        {
            if (inKnockback)
            {
                m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, minFallSpeed);
            }
            else if (jumpFrame <= 0)
            {
                m_VerticalSpeed = minFallSpeed;
            }
            else
            {
                SetVerticalSpeed();
            }
        }

        jumpTriggered = false;
    }
    bool inKnockback;
    protected override IEnumerator Knockback(KnockBackResponse knockback, AFighterPhysicsObject damageSource)
    {
        inKnockback = true;
        yield return StartCoroutine(base.Knockback(knockback, damageSource));
        inKnockback = false;
    }

    bool autoTarget;
    void OnTargetingLockChanged(bool val)
    {
        if (val)
        {
            autoTarget = !autoTarget;
        }
    }

    Damageable diTarget;
    void HorizontalDI(float value)
    {
        if (currentTarget != null)
        {
            diTarget = currentTarget;
        }
        else if (diTarget == null)
        {
            diTarget = GetClosestTarget();
        }

        if (diTarget == null)
        {
            return;
        }

        var currentRotation = transform.rotation;
        transform.LookAt(diTarget.transform);
        var directRotation = transform.rotation;
        var deltaAngle = Mathf.Abs(Mathf.DeltaAngle(m_TargetRotation.eulerAngles.y, directRotation.eulerAngles.y));
        diStick.x = 1 - (2 * deltaAngle / 180);
        transform.rotation = currentRotation;
    }

    void VerticalDI(float value)
    {
        diStick.y = value;
    }

    void CameraChanged(Vector2 dpad)
    {
        if (dpad.y < 0)
        {
            CameraDown();
        }
        else if (dpad.y > 0)
        {
            CameraUp();
        }

        else if (dpad.x < 0)
        {
            CameraLeft();
        }
        else if (dpad.x > 0)
        {
            CameraRight();
        }
    }

    public void OnEnable()
    {
        StartCoroutine(otherJawnFollow());
    }

    IEnumerator otherJawnFollow()
    {
        if (otherChar != null)
        {
            while (gameObject.activeSelf)
            {
                otherChar.transform.position = transform.position;
                otherChar.transform.rotation = transform.rotation;
                yield return null;
            }
        }
    }

    public FighterController otherChar;
    bool characterIs = true;
    void SwapCharacters(bool val)
    {
        if (val)
        {
            GeniousEffectManager.instance.GenerateSwapCharacterEffect(transform.position);
            otherChar.smashInput.rewiredPlayer = smashInput.rewiredPlayer;
            otherChar.smashInput.playerNumber = smashInput.playerNumber;
            otherChar.otherChar = this;
            smashInput.rewiredPlayer = null;
            gameObject.SetActive(false);
            otherChar.transform.position = transform.position;
            otherChar.percent = percent;
            otherChar.gameObject.SetActive(true);

            Debug.Log("Swapped to " + otherChar.fighterData.displayName);
        }
    }

    void SetVerticalSpeed(bool? overrideRising = null)
    {
        float x = GeniousSettings.fixedFrame - jumpFrame + 1;
        bool rising = overrideRising.HasValue ? overrideRising.Value : m_VerticalSpeed > 0;
        float lastFrameDisplacement = GetVerticalDisplacement(x - 1, rising);
        float thisFrameDisplacement = GetVerticalDisplacement(x, rising);
        if (m_VerticalSpeed > 0 && thisFrameDisplacement - lastFrameDisplacement <= 0)
        {
            thisFrameDisplacement = GetVerticalDisplacement(x, false);
        }
        m_VerticalSpeed = thisFrameDisplacement - lastFrameDisplacement;
        m_VerticalSpeed *= GeniousSettings.instance.jumpHeightScalar;// SmashSettings.instance.verticalSmashUnitScalar;
        //m_VerticalSpeed /= Time.deltaTime;
    }

    float GetVerticalDisplacement(float x, bool rising)
    {
        float ret;
        int lastFrame = shortHop ? fighterData.attributes.sh_air_time : fighterData.attributes.fh_air_time;
        lastFrame = Mathf.RoundToInt((float)lastFrame / GeniousSettings.instance.jumpSpeedScalar);
        float height = (float)(shortHop ? fighterData.attributes.hop_height : fighterData.attributes.jump_height);
        //height *= SmashSettings.instance.jumpScalar;
        // height *= transform.localScale.x;
        var fallSpeed = fighterData.attributes.fall_speed;// * transform.localScale.x;
        float apexFrame = lastFrame - (height / fallSpeed); //SH: 41-10.25=30.75
        //apexFrame = -(Mathf.Sqrt(-height/) - lastFrame);

        float a = GetJumpCoefficient(rising, height, apexFrame, lastFrame);
        ret = CalculateVertexFormQuadratic(a, x, apexFrame, height);
        return ret;
    }

    float CalculateVertexFormQuadratic(float a, float x, float h, float k)
    {
        var ret = a * Mathf.Pow(x - h, 2) + k;
        // Debug.Log(ret + " = " + a + "(" + x + " - " + h + ")^2 + " + k);
        return ret;
    }

    float GetJumpCoefficient(bool rising, float jumpHeight, float apexFrame, int lastFrame)
    {
        float ret;
        if (rising)
        {
            ret = -jumpHeight / Mathf.Pow(apexFrame, 2);
            // Debug.Log("RisingA = " + ret + " = -" + jumpHeight + " / " + apexFrame + "^2");
        }
        else
        {
            ret = -jumpHeight / Mathf.Pow((lastFrame - apexFrame), 2);
            // Debug.Log("FallingA = " + ret + " = -" + jumpHeight + " / (" + lastFrame + "-" + apexFrame + ")^2");
        }

        return ret;
    }

    public void RegisterSmashInput(FighterPlayerInput input)
    {
        smashInput = input;
    }

    MeleeWeaponTrail[] weaponTrails;
    Dictionary<ushort, HitboxEntry> hitboxEntries = new Dictionary<ushort, HitboxEntry>();
    Vector3 maxShieldScale;
    protected void Awake()
    {
        model = Instantiate(fighterData.model, transform.position, transform.rotation, transform);
        model.transform.localEulerAngles = fighterData.model.transform.localEulerAngles;
        model.name = "Model";
        transform.name += " [" + fighterData.name + "]";
        hitboxYOffset = model.hitboxOffsetOrCharCenter;
        //go.transform.DetachChildren();
        for (var i = 0; i < model.transform.childCount; i++)
        {
            var t = model.transform.GetChild(i);
            GenericCoroutineManager.instance.RunAfterFrame(() =>
            {
               // t.SetParent(transform, true);
            });
        }
        m_CharCtrl = GetComponent<CharacterController>();
        CapsuleCollider cc = GetComponent<CapsuleCollider>();
        var charControllerReference = model.GetComponent<CharacterController>();
        m_CharCtrl.center = cc.center = Quaternion.Euler(fighterData.model.rotationAdjustment) * (charControllerReference.center * charControllerReference.transform.localScale.x);
        m_CharCtrl.height = charControllerReference.height * charControllerReference.transform.localScale.x;
        cc.height = m_CharCtrl.height + GeniousSettings.instance.pushColliderHeightOffset;
        m_CharCtrl.radius = charControllerReference.radius * charControllerReference.transform.localScale.x;
        m_CharCtrl.stepOffset = m_CharCtrl.height / 4f;
        Destroy(charControllerReference);

        cc.radius = m_CharCtrl.radius + GeniousSettings.instance.pushColliderRadiusOffset;
       // GenericCoroutineManager.instance.RunInFrames(1, () =>
        {
            m_Animator = GetComponent<Animator>();// go.AddComponent<Animator>();
            m_Animator.avatar = fighterData.avatar;
            m_Animator.updateMode = AnimatorUpdateMode.AnimatePhysics;
            m_Animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            m_Animator.applyRootMotion = true;
            m_Animator.runtimeAnimatorController = fighterData.runtimeAnimatorController;
            //Destroy(go);
        }
        //);
       
        if (personalSpace == null)
        {
            personalSpace = GetComponent<CapsuleCollider>();
        }
        RegisterStateCallback(FighterState.Guard, (val) =>
        {
            shield.SetActive(val);
        });

        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            landingDirectionTransform = transform.FindChildRecursive(fighterData.boneRoot);
        });

        fighterData.Init();
        foreach (var entry in fighterData.hitboxData.entries)
        {
            hitboxEntries[entry.animationId] = entry;
        }

        weaponTrails = GetComponentsInChildren<MeleeWeaponTrail>();
    }

    void InitializeSmashStateAnimatorReferences()
    {
        foreach (var val in Enum.GetValues(typeof(FighterState)))
        {
            smashPlayerStateAnimatorReferences[(FighterState)val] = Animator.StringToHash("State_" + val.ToString());
        }
    }

    Queue<UnparsedMove> moveQueue = new Queue<UnparsedMove>();
    ATKD atkd;
    protected override void Start()
    {

        InitializeSmashStateAnimatorReferences();
        Time.timeScale = 0f;

        if (GeniousSocketManager.instance.IsConnected)
        {
            Time.timeScale = 1f;
            //  GeniousSettings.instance.InitializeMoveConversions(fighterData);
        }
        else
        {
            GeniousSocketManager.instance.onConnected += () =>
            {
              //   GeniousSettings.instance.InitializeMoveConversions(fighterData);
                Time.timeScale = 1f;
             //moveQueue = new Queue<UnparsedMove>(frameData.unparsedMoves);
             //SmashSocketManager.instance.RequestMoveConversion(moveQueue.Peek(), HandleMoveResponse);
            };
        }

        base.Start();
        if (hitbox == null)
        {
            hitbox = GetComponentInChildren<HitBox>();
        }

        if (hitbox == null)
        {
            Debug.LogError("Character Prefab for " + fighterData.characterName +" needs a hitbox. Probably on the TransN object.", fighterData.model);
        }

        m_CharCtrl.stepOffset *= transform.lossyScale.y;

        SetHitboxActive(false);
        #region Animator Input Parameters
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            shield = GetComponentInChildren<FighterShield>(true);
            shield.onShieldBreak += TriggerShieldBreak;
            SetShield(false);
            AddToAttackDetected(RewiredInputActions.instance.Jab);
            AddToAttackDetected(RewiredInputActions.instance.Smash);
            AddToAttackDetected(RewiredInputActions.instance.Special);
            AddToAttackDetected(RewiredInputActions.instance.Block);

            #endregion

            foreach(var action in ReInput.mapping.Actions)
            {
                if (action.type == InputActionType.Button)
                {
                    SetupButton(action);
                }
                else if (action.type == InputActionType.Axis)
                {
                    SetupAxis<float>(action);
                }
            }
           /* foreach (var buttonAction in smashInput allButtonActions)
            {
                SetupButton(buttonAction);
            }

            foreach (var axisAction in smashInput.all1dAxisActions)
            {
                SetupAxis<float>(axisAction);
            }
            */

           /* foreach (var axisAction in smashInput.all2dAxisActions)
            {
                SetupAxis<Vector2>(axisAction);
            }*/


            #region Local Input Callbacks
            smashInput.AppendAction<bool>(RewiredInputActions.instance.MoveForward, OnMoveForward);
            smashInput.AppendAction<bool>(RewiredInputActions.instance.Jump, OnJump);
            smashInput.AppendAction<bool>(RewiredInputActions.instance.ToggleTargetLock, OnTargetingLockChanged);
            smashInput.AppendAction<Vector2>(RewiredInputActions.instance.DpadHorizontal, CameraChanged,RewiredInputActions.instance.DpadVertical);
            smashInput.AppendAction<float>(RewiredInputActions.instance.MovementVertical, VerticalDI);
            smashInput.AppendAction<float>(RewiredInputActions.instance.MovementHorizontal, HorizontalDI);
            smashInput.AppendAction<bool>(RewiredInputActions.instance.ToggleCamera, ToggleCamera);
            smashInput.AppendAction<bool>(RewiredInputActions.instance.GetUp, TriggerGetUp);
            smashInput.AppendAction<bool>(RewiredInputActions.instance.Block, SetShield);
            smashInput.AppendAction<bool>(RewiredInputActions.instance.Special, OnSpecial);
            smashInput.AppendAction<bool>(RewiredInputActions.instance.Smash, OnSmash);
            smashInput.AppendAction<bool>(RewiredInputActions.instance.NextCharacter, SwapCharacters);
            smashInput.AppendComboAction(SetUpTilt, RewiredInputActions.instance.Jab, RewiredInputActions.instance.Jump);
            smashInput.AppendComboAction(SetUpSmash, RewiredInputActions.instance.Smash, RewiredInputActions.instance.Jump);
            smashInput.AppendComboAction(SetUpSpecial, RewiredInputActions.instance.Special, RewiredInputActions.instance.Jump);

            smashInput.AppendComboAction(SetDownTilt, RewiredInputActions.instance.Jab, RewiredInputActions.instance.Smash);

            if (rightStickSmash)
            {
                smashInput.AppendComboAction(SetDownSmash, RewiredInputActions.instance.Smash, RewiredInputActions.instance.Special);
                smashInput.AppendAction<Vector2>(RewiredInputActions.instance.AltAxisHorizontal, (val) =>
                {
                    SetAltAxis(val, m_DownSmash, m_ForwardSmash);
                }, RewiredInputActions.instance.AltAxisVertical);
            }
            else
            {
                smashInput.AppendComboAction(SetDownSpecial, RewiredInputActions.instance.Smash, RewiredInputActions.instance.Special);
                smashInput.AppendAction<Vector2>(RewiredInputActions.instance.AltAxisHorizontal, (val) =>
                {
                    SetAltAxis(val, m_DownSpecial, m_ForwardSpecial);
                }, RewiredInputActions.instance.AltAxisVertical);
            }
            smashInput.AppendComboAction(SetDownSmash, RewiredInputActions.instance.Special, RewiredInputActions.instance.Jump);

            /*smashInput.onButton[smashInput.actions.CameraDown] += CameraDown;
            smashInput.onButton[smashInput.actions.CameraUp] += CameraUp;
            smashInput.onButton[smashInput.actions.CameraLeft] += CameraSide;
            smashInput.onButton[smashInput.actions.CameraRight] += CameraSide;*/
            #endregion

        });
    }
    bool rightStickAllowed = true;
    bool rightStickHorizontalAllowed = true;
    void SetAltAxis(Vector2 axis, int? verticalAction = null, int? horizontalAction = null)
    {
        float cutoff = GeniousSettings.instance.rightStickMagnitudeCutoff;
        if (rightStickAllowed)
        {
            if (axis.y >= cutoff && verticalAction.HasValue)
            {
                SetTrigger(verticalAction.Value);
                rightStickAllowed = false;
            }
            else if (horizontalAction.HasValue && axis.x >= cutoff)
            {
                SetTrigger(horizontalAction.Value);
            }
        }
        else if (axis.y < cutoff && axis.x < cutoff)
        {
            rightStickAllowed = true;
        }
    }

    void OnSpecial(bool val)
    {
        if (val)
        {
            DirectionalAttack(m_ForwardSpecial);
        }
    }

    void OnSmash(bool val)
    {
        if (val)
        {
            DirectionalAttack(m_ForwardSmash);
        }
    }

    void DirectionalAttack(int attackHash)
    {
        if (inputMagnitude > GeniousSettings.instance.directionalAttackInputMagnitudeCutoff)
        {
            SetTrigger(attackHash, true);
        }
    }
    void SetUpTilt()
    {
        m_Animator.SetBool("Tilt", true);
        SetTrigger(m_UpTilt, true);
    }

    void SetDownTilt()
    {
        m_Animator.SetBool("Tilt", true);

        SetTrigger(m_DownTilt, true);
    }

    void SetUpSmash()
    {
        m_Animator.SetBool("Smash", true);
        SetTrigger(m_UpSmash, true);
    }

    void SetDownSmash()
    {
        m_Animator.SetBool("Smash", true);

        SetTrigger(m_DownSmash, true);
    }

    void SetUpSpecial()
    {
        m_Animator.SetBool("Special", true);
        SetTrigger(m_UpSpecial, true);
    }

    void SetDownSpecial()
    {
        m_Animator.SetBool("Special", true);
        SetTrigger(m_DownSpecial, true);
    }

    void CameraDown()//bool val)
    {
        VirtualCameraManager.instance.SetCameraIndex(3);
    }

    void CameraUp()//bool val)
    {
        VirtualCameraManager.instance.SetCameraIndex(0);
    }

    void CameraLeft()//bool val)
    {
        VirtualCameraManager.instance.SetCameraIndex(1);
    }

    void CameraRight()//bool val)
    {
        VirtualCameraManager.instance.SetCameraIndex(2);
    }

    protected override bool IsWeaponEquiped()
    {
        return true;
    }

    void AddToAttackDetected(InputAction action)
    {
        attackDetectedActions.Add(action);
    }

    void SetupAxis<T>(InputAction action)
    {
        string actionNameNoSpaces = action.name.Replace(" ", "");
        int animatorHash = Animator.StringToHash("InputAxis_" + actionNameNoSpaces);
        int startAnimatorHash = Animator.StringToHash("Input_" + actionNameNoSpaces + "Started");
        int animatorAxisHash = Animator.StringToHash("InputAxis_" + actionNameNoSpaces);
        int animatorXAxisHash = Animator.StringToHash("InputAxis_" + actionNameNoSpaces + "XAxis");
        int animatorYAxisHash = Animator.StringToHash("InputAxis_" + actionNameNoSpaces + "YAxis");

        int endAnimatorHash = Animator.StringToHash("Input_" + actionNameNoSpaces + "Ended");
        inputTimeHashKeys[action] = Animator.StringToHash("Input_" + actionNameNoSpaces + "TimePressed");
        smashInput.AppendAction<T>(action, (val) =>
        {
            // Debug.Log("Val: " + val + " for " + action.name);
            if (typeof(T) == typeof(float))
            {
                var value = (float)Convert.ChangeType(val, typeof(float));
                if (value != 0)
                {
                    SetTrigger(startAnimatorHash);
                }
                else
                {
                    SetTrigger(endAnimatorHash);
                    m_Animator.SetFloat(inputTimeHashKeys[action], 0f);
                }

                //Debug.Log("Setting " + action.name + " to " + value);
                m_Animator.SetFloat(animatorAxisHash, value);
                m_Animator.SetBool(animatorHash, value != 0);
                Debug.LogWarning(actionNameNoSpaces);
            }
            else
            {
                var value = (Vector2)Convert.ChangeType(val, typeof(Vector2));
                if (value != Vector2.zero)
                {
                    SetTrigger(startAnimatorHash);
                }
                else
                {
                    SetTrigger(endAnimatorHash);
                    m_Animator.SetFloat(inputTimeHashKeys[action], 0f);
                }

                m_Animator.SetFloat(animatorAxisHash, value.magnitude);
                m_Animator.SetFloat(animatorXAxisHash, value.x);
                m_Animator.SetFloat(animatorYAxisHash, value.y);
                m_Animator.SetBool(animatorHash, value != Vector2.zero);
            }
        });
    }

    bool _attackDetected;
    bool attackDetected {
        get {
            return _attackDetected;
        }
        set {
            _attackDetected = value;
            m_Animator.SetBool(m_AttackDetected, value);
        }
    }
    int buttonInputsDetectedThisFrame;
    void SetupButton(InputAction action)
    {
        int animatorHash = Animator.StringToHash("InputButton_" + action.name);
        int startAnimatorHash = Animator.StringToHash("Input_" + action.name + "Started");
        int endAnimatorHash = Animator.StringToHash("Input_" + action.name + "Ended");
        inputTimeHashKeys[action] = Animator.StringToHash("Input_" + action.name + "TimePressed");

        smashInput.AppendAction<bool>(action, (val) =>
        {
            if (val)
            {
                m_Animator.SetTrigger(startAnimatorHash);
                buttonInputsDetectedThisFrame++;
                bool isAttack = attackDetectedActions.Contains(action);
                if (isAttack)
                {
                    attackDetected = true;
                    m_Animator.SetTrigger(m_AttackStarted);
                }

                onNextAnimatorFrame += () => {
                    m_Animator.ResetTrigger(startAnimatorHash);
                    if (isAttack)
                    {
                        m_Animator.ResetTrigger(m_AttackStarted);
                    }
                };

                activeButtons[action] = Time.time;
            }
            else
            {
                SetTrigger(endAnimatorHash);

                // m_Animator.SetTrigger(releasedHash);
                activeButtons.Remove(action);
                bool attackStillDetected = false; ;
                foreach (var a in activeButtons)
                {
                    if (attackDetectedActions.Contains(a.Key))
                    {
                        attackStillDetected = true;
                        break;
                    }
                }
                if (!attackStillDetected)
                {
                    attackDetected = false;
                }
                m_Animator.SetFloat(inputTimeHashKeys[action], 0f);
            }

            m_Animator.SetBool(animatorHash, val);
        });
    }

    public Vector3 forwardMovement;
    protected override void FixedUpdate()
    {
        if (state == FighterState.Dead)
        {
            return;
        }
        HandlePushAway();

        buttonInputsDetectedThisFrame = 0;
        base.FixedUpdate();
        UpdateInputAnimatorValues();

       // m_Animator.SetBool(m_ExitLocomotion, attackDetected || !m_IsGrounded || Mathf.Abs(inputMagnitude) < inputDeadZone);
        UpdatePositionFromRootMotion();
        switch (state)
        {
            case FighterState.Normal:
                UpdatePositionFromMoveInput();
                break;
            case FighterState.Reeling:
                HandleHitStun();
                break;
            case FighterState.Tumbling:
                HandleHitStun();
                break;
            case FighterState.Buried:
                break;
            case FighterState.Frozen:
                break;
            case FighterState.Helpless:
                break;
            case FighterState.GettingUp:
                break;
            case FighterState.Stunned:
                HandleStunned();
                break;
            case FighterState.Floored:
                HandleFlooredOptions();
                break;
            case FighterState.Guard:
                HandleGuard(true);
                break;
            case FighterState.Dodging:
                break;
            default:
                Debug.LogError("This state [" + state + "] isn't handled yet");
                break;
        }

        if (FighterState.Guard != state)
        {
            HandleGuard(false);
        }

        GenericCoroutineManager.instance.RunAfterFrame(UpdateGroundedState);
        //  Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
    }

    bool CheckRoll()
    {
        if (inputMagnitude > 0.6f)
        {
            return true;
        }
        return false;
    }

    public override void RegisterHurt(HitPoint hitPoint, Move move)
    {
        if (state != FighterState.Guard)
        {
            SetNormalState();
        }

        base.RegisterHurt(hitPoint, move);
    }

    int shieldBreakFramesLeft;
    void TriggerShieldBreak()
    {
        state = FighterState.Stunned;
        StartCoroutine(MonitorShieldBreak());
    }

    //Source: https://www.reddit.com/r/smashbros/comments/879cny/what_is_the_formula_for_how_long_the_shieldbreak/

    IEnumerator MonitorShieldBreak()
    {
        shieldBreakFramesLeft = Mathf.RoundToInt(GeniousSettings.instance.maxShieldBreakFrames - percent * GeniousSettings.instance.shieldBreakPercentReductionMultiplier);
        while (shieldBreakFramesLeft > 0)
        {
            yield return new WaitForEndOfFrame();
            shieldBreakFramesLeft--;
            //Debug.Log(shieldBreakFramesLeft + " more franmes");
        }

        SetNormalState();
    }

    void HandleGuard(bool inGuard)
    {
        if (shield == null)
        {
            return;
        }
        if (inGuard)
        {
            if (!CheckRoll())
            {
                shield.AdjustShieldHealth(-GeniousSettings.instance.shieldDepletionPerFrame);
            }
        }
        else
        {
            shield.AdjustShieldHealth(GeniousSettings.instance.shieldRegenerationPerFrame);
        }

        //shield.SetActive(inGuard);
    }

    void HandleStunned()//Shield break
    {
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            if (buttonInputsDetectedThisFrame > 0)
            {
                Debug.Log("Reducing shield stun by " + (GeniousSettings.instance.shieldBreakFrameReductionPerButtonInput * buttonInputsDetectedThisFrame));
            }

            shieldBreakFramesLeft -= GeniousSettings.instance.shieldBreakFrameReductionPerButtonInput * buttonInputsDetectedThisFrame;
        });
    }

    void HandleHitStun()
    {
        if (hitStunned)
        {
            if ((hitStunAerialCancellable && (attackDetected || JumpInput))
                || (hitStunAirDodgeCancellable && smashInput.IsButton(RewiredInputActions.instance.AirDodge))
                || (!inKnockback && m_IsGrounded))
            {
                Debug.Log(GeniousSettings.fixedFrame + ".) Grounded: " + m_IsGrounded);
                //                GenericCoroutineManager.instance.RunInFixedUpdateFrames(1, () => { CancelHitStun(); });
                CancelHitStun();
            }
        }
    }

    void HandlePushAway()
    {
        if (pushAwayFrom != null)
        {
            Vector3 direction = transform.position - pushAwayFrom.position;
            direction.y = 0;
            Vector3 translation = (direction).normalized * GeniousSettings.instance.pushAwaySpeed * Time.fixedDeltaTime;
            MoveDirect(translation);
        }
        pushAwayFrom = null;
    }

    void HandleFlooredOptions()
    {
        if (state == FighterState.Floored)
        {
            //If we change this inputMagnitude here, we need to update the transitions from "Floored" to "Roll" states in Mecanim
            if (attackDetected || inputMagnitude > 0.4f)
            {
                TriggerGetUp();
            }
        }
    }

    public override void TriggerTumbling()
    {
        state = FighterState.Tumbling;
    }

    public override void TriggerReeling()
    {
        state = FighterState.Reeling;
    }

    public void TriggerGetUpComplete()
    {
        SetNormalState();
    }
    public void TriggerGetUp(bool value)
    {
        if (value)
        {
            TriggerGetUp();
        }
    }

    public override void TriggerGetUp()
    {
        GettingUp();
    }

    public override void GettingUp()
    {
        if (state == FighterState.Floored)
        {
            state = FighterState.GettingUp;
        }
    }

    protected override void SetFloored()
    {
        state = FighterState.Floored;
    }

    protected override void SetNormalState()
    {
        state = FighterState.Normal;
    }


    protected override void MoveCharacterFromAnimator(Vector3 movement)
    {
        base.MoveCharacter(movement); //Not very polymorphic
    }
    protected override Vector3 GetAirMovement(Vector3 movement)
    {
        //Handled elsewhere
        return movement;
    }

    protected override void MoveCharacter(Vector3 movement)
    {
        var positionBefore = transform.position;
        var startRotation = transform.rotation;
        if (!isMovingForward)
        {
            transform.rotation = m_TargetRotation;
        }
//        MoveDirect(transform.up * movement.y);
        MoveDirect(movement);
        transform.rotation = startRotation;
    }

    public override void MoveDirect(Vector3 motion)
    {
        m_CharCtrl.Move(motion);
    }

    protected override bool IsOrientationUpdated()
    {
        return true;
    }

    protected override void UpdateHash()
    {
        //Ignore this. This empty function is neccessary for proper function
    }

    float inputMagnitude;

    void UpdateInputAnimatorValues()
    {
        m_Animator.SetFloat(m_HorizontalInput, MoveInput.x);
        m_Animator.SetFloat(m_VerticalInput, MoveInput.y);
        inputMagnitude = (Mathf.Sqrt(MoveInput.x * MoveInput.x + MoveInput.y * MoveInput.y));
        m_Animator.SetFloat(m_InputMagnitude, inputMagnitude);// MoveInput.magnitude);
        if (m_IsGrounded)
        {
            m_Animator.SetInteger(m_JumpsLeft, fighterData.attributes.jumps);
            m_Animator.SetBool(m_AirDodged, false);
        }
        else
        { }

        foreach (var pair in activeButtons)
        {
            m_Animator.SetFloat(inputTimeHashKeys[pair.Key], Time.time - pair.Value);
        }
    }

    public bool canMove { get; protected set;} = true;
    void UpdatePositionFromMoveInput()
    {
        if (IsMoveInput)
        {
            if (canMove)
            {
                var scalar = m_IsGrounded ? 1f : airMovementScalar;
                // if (!m_IsGrounded)
                {
                    MoveCharacter(GetFixedDeltaTimeTranslation(scalar));
                }
            }
        }
    }

    //I don't understand how root motion is supposed to work so this is my workaround
    protected bool UpdatePositionFromRootMotion()
    {

        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            if (ignoreRootMotionOnThisFrame == GeniousSettings.fixedFrame)
            {
                return;
            }
            
            if (animationMovementOffset != Vector3.zero)
            {
               // var translation = Quaternion.Euler(-90, 0, 0) * (animationMovementOffset - lastOffset) * transform.localScale.x;
                var translation = (animationMovementOffset - lastOffset) * transform.localScale.x;

                Vector3 newTranslation = transform.TransformDirection(translation);

                if (translation.y != 0)
                {
                     //SmashSettings.Log("Translation: " + translation.y + "(" + animationMovementOffset + ")");
                }
                m_CharCtrl.Move(newTranslation);
                lastOffset = animationMovementOffset;
            }
        });

        return Vector3.zero != animationMovementOffset;
    }

    Vector3 lastOffset;
    public Vector3 animationMovementOffset {
        get {
            return ignoreRootMotionOnThisFrame == GeniousSettings.fixedFrame ? Vector3.zero : model.scaledAnimationMovementOffset;
        }
        set {
            model.animationMovementOffset = value; }
    }

    public bool resetPosition;// direction;
    public void OnMovingAnimationStarted()
    {
        ResetLastOffset();
    }

    public void OnMovingAnimationEnded()
    {
        ResetLastOffset();
    }

    void ResetLastOffset()
    {
        //Debug.Log("ResetLastOffset!");
        lastOffset = Vector3.zero;
    }

    Vector3 GetFixedDeltaTimeTranslation(float scalar, float? forwardSpeed = null)
    {
        if (forwardSpeed == null)
            forwardSpeed = m_ForwardSpeed;

        //        return -transform.forward * forwardSpeed.Value * Time.fixedDeltaTime * scalar;
        return transform.forward * forwardSpeed.Value * Time.fixedDeltaTime * scalar;
    }

    List<Damageable> smashTargets;

    Damageable currentTarget;
    protected override void SetTargetRotation()
    {
        if (canRotate)
        {
            base.SetTargetRotation();

            bool shouldSetRotaion = true;
            bool targetButtonDown = smashInput.Get<bool>(RewiredInputActions.instance.Target);
            if ((targetButtonDown && !autoTarget) || (autoTarget && !targetButtonDown))
            {
                if (currentTarget == null)
                {
                    currentTarget = GetClosestTarget();
                }
                if (currentTarget != null)
                {
                    shouldSetRotaion = false;
                    transform.rotation = Quaternion.LookRotation(currentTarget.transform.position - transform.position);// * Quaternion.Euler(90, 90, 90);
                    transform.SetXLocalRotation(0f); //always look eye! (forward)
                }
                else
                {
                    // Debug.Log("No targets found.");
                }
            }
            else
            {
                currentTarget = null;
            }

           // m_TargetRotation *= Quaternion.Euler(90, 90, 90);// Vector2.right * 90);
            if (shouldSetRotaion && !isMovingForward && Vector2.zero != MoveInput)
            {
                transform.rotation = m_TargetRotation;
            }
        }
    }

    protected override void UpdateOrientation()
    {
        //Needs to be empty so orientation is updated in our local SetTargetRotation even if no movement calculated
//        base.UpdateOrientation();
    }

    public float targetsRange = 50f;
    Damageable GetClosestTarget()
    {
        if (smashTargets == null)
        {
            smashTargets = new List<Damageable>();
            foreach(var po in FindObjectsOfType<AFighterPhysicsObject>())
            {
                if (po != this)
                {
                    smashTargets.Add(po.gameObject.GetComponent<Damageable>());
                }
            }
        }

        bool resetEveryTurn = smashTargets.Count <= 0;
        if (resetEveryTurn)
        {
            foreach(var c in Physics.OverlapSphere(transform.position, targetsRange))
            {
                var d = c.gameObject.GetComponent<Damageable>();
                if (d != null)
                {
                    smashTargets.Add(d);
                }
            }
        }



        Damageable ret = null;
        float minDistance = Mathf.Infinity;
        foreach(var target in smashTargets)
        {
            if (!target.gameObject.activeInHierarchy || !target.enabled || target.gameObject == this.gameObject)
            {
                continue;
            }
            var distance = Vector3.Distance(target.transform.position, transform.position);
            if (distance < minDistance)
            {
                distance = minDistance;
                ret = target;
            }
        }

        if (resetEveryTurn)
        {
            smashTargets.Clear();
        }
        return ret;
    }
}