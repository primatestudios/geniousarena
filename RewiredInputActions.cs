using Rewired;
using UnityEngine;
public class RewiredInputActions : MonoSingleton<RewiredInputActions>
{
    public static string[] AllButtonActions = new string[] {"Jump","Target","Smash","Special","ToggleCamera","GetUp","Block","ToggleTargetLock","Jab","AirDodge","MoveForward","NextCharacter"};
	public static string[] AllAxisActions = new string[] {"MovementHorizontal","MovementVertical","DpadHorizontal","DpadVertical","UpDownAxis","AltAxisHorizontal","AltAxisVertical"};
	public InputAction Jump {get; protected set;}
	public InputAction Target {get; protected set;}
	public InputAction Smash {get; protected set;}
	public InputAction Special {get; protected set;}
	public InputAction ToggleCamera {get; protected set;}
	public InputAction MovementHorizontal {get; protected set;}
	public InputAction MovementVertical {get; protected set;}
	public InputAction GetUp {get; protected set;}
	public InputAction Block {get; protected set;}
	public InputAction ToggleTargetLock {get; protected set;}
	public InputAction DpadHorizontal {get; protected set;}
	public InputAction DpadVertical {get; protected set;}
	public InputAction UpDownAxis {get; protected set;}
	public InputAction Jab {get; protected set;}
	public InputAction AltAxisHorizontal {get; protected set;}
	public InputAction AltAxisVertical {get; protected set;}
	public InputAction AirDodge {get; protected set;}
	public InputAction MoveForward {get; protected set;}
	public InputAction NextCharacter {get; protected set;}
	protected override void Awake()
	{
		base.Awake();
		foreach(var action in ReInput.mapping.Actions)
		{
			if (action.name == "Jump"){Jump=action;}
			if (action.name == "Target"){Target=action;}
			if (action.name == "Smash"){Smash=action;}
			if (action.name == "Special"){Special=action;}
			if (action.name == "ToggleCamera"){ToggleCamera=action;}
			if (action.name == "Movement Horizontal"){MovementHorizontal=action;}
			if (action.name == "Movement Vertical"){MovementVertical=action;}
			if (action.name == "GetUp"){GetUp=action;}
			if (action.name == "Block"){Block=action;}
			if (action.name == "ToggleTargetLock"){ToggleTargetLock=action;}
			if (action.name == "Dpad Horizontal"){DpadHorizontal=action;}
			if (action.name == "Dpad Vertical"){DpadVertical=action;}
			if (action.name == "UpDownAxis"){UpDownAxis=action;}
			if (action.name == "Jab"){Jab=action;}
			if (action.name == "AltAxis Horizontal"){AltAxisHorizontal=action;}
			if (action.name == "AltAxis Vertical"){AltAxisVertical=action;}
			if (action.name == "AirDodge"){AirDodge=action;}
			if (action.name == "MoveForward"){MoveForward=action;}
			if (action.name == "NextCharacter"){NextCharacter=action;}
		}
	}
}