using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

public class GenericCoroutineManager : MonoSingleton<GenericCoroutineManager>
{

    public Action runOnLateUpdate;
    public Action runOnUpdate;

    public Action runOnNextUpdate;
    public Action runOnNextLateUpdate;

    Thread mainThread;

    public Coroutine RunInSeconds(float numSeconds, Action action)
    {
        if (mainThread != Thread.CurrentThread)
        {
            Debug.Log("WARNING: Returning null Coroutine because this isn't called from tyhe main thread");
            runOnNextUpdate += () => { RunInSeconds(numSeconds, action); };
            return null;
        }
        return StartCoroutine(RunInSecondsCoroutine(numSeconds, action));
    }

    public Coroutine RunInSecondsRealtime(float numSeconds, Action action)
    {
        if (mainThread != Thread.CurrentThread)
        {
            Debug.Log("WARNING: Returning null Coroutine because this isn't called from tyhe main thread");
            runOnNextUpdate += () => { RunInSecondsRealtime(numSeconds, action); };
            return null;
        }
        return StartCoroutine(RunInSecondsRealtimeCoroutine(numSeconds, action));
    }

    protected override void Awake()
    {
        base.Awake();
        gameObject.DontDestroyOnLoad();
        mainThread = Thread.CurrentThread;
    }
    public Coroutine RunInFrames(int numFrames, Action action)
    {
        if (mainThread != Thread.CurrentThread)
        {
            Debug.Log("WARNING: Returning null Coroutine because this isn't called from tyhe main thread");
            runOnNextLateUpdate += () => { RunInFrames(numFrames, action); };
            return null;
        }
        return StartCoroutine(RunInFramesCoroutine(numFrames, action));
    }

    public Coroutine RunAfterFrame(Action action)
    {
        return ExecuteAfterFrame(action);
    }

    public Coroutine ExecuteAfterFrame(Action action)
    {
        if (mainThread != Thread.CurrentThread)
        {
            Debug.Log("WARNING: Returning null Coroutine because this isn't called from the main thread");
            runOnNextLateUpdate += () => { ExecuteAfterFrame(action); };
            return null;
        }
        return StartCoroutine(ExecuteAfterFrameCoroutine(action));
    }

    public Coroutine ExecuteAtEndOfFrame(Action action)
    {
        return ExecuteAfterFrame(action);
    }

    public Coroutine RunAfterFixedUpdateFrame(Action action)
    {
        return StartCoroutine(RunAfterFixedUpdateCoroutine(action));
    }

    public Coroutine RunInFixedUpdateFrames(int frames, Action action)
    {
        return StartCoroutine(RunInFixedUpdateFramesCoroutine(frames, action));

    }

    public Coroutine RunOnFixedUpdateFrame(Action action)
    {
        return StartCoroutine(RunOnFixedUpdateCoroutine(action));
    }

    public new void StopCoroutine(Coroutine c)
    {
        base.StopCoroutine(c);
    }

    IEnumerator RunAfterFixedUpdateCoroutine(Action action)
    {
        yield return new WaitForFixedUpdate();
        yield return new WaitForEndOfFrame();
        action.Invoke();
    }

    IEnumerator RunOnFixedUpdateCoroutine(Action action)
    {
        yield return new WaitForFixedUpdate();
        action.Invoke();
    }

    IEnumerator RunInFixedUpdateFramesCoroutine(int frames, Action action)
    { 
        while(frames-- > 0)
        {
            yield return new WaitForFixedUpdate();
        }
        action.Invoke();
    }

    IEnumerator RunInFramesCoroutine(int numFrames, Action action)
    {
        for (int i = 0; i < numFrames; i++)
        {
            yield return null;
        }
        action();
    }

    IEnumerator RunInSecondsCoroutine(float seconds, Action action)
    {
        yield return new WaitForSeconds(seconds);
        action();
    }

    IEnumerator RunInSecondsRealtimeCoroutine(float seconds, Action action)
    {
        yield return new WaitForSecondsRealtime(seconds);
        action();
    }
    IEnumerator ExecuteAfterFrameCoroutine(Action action)
    {
        yield return new WaitForEndOfFrame();
        action.Invoke();
    }

    void Update()
    {
        if (runOnUpdate != null)
            runOnUpdate.Invoke();

        if (runOnNextUpdate != null)
        {
            runOnNextUpdate.Invoke();
            runOnNextUpdate = null;
        }
    }

    void LateUpdate()
    {
        if (runOnLateUpdate != null)
        {
            runOnLateUpdate.Invoke();
        }

        if (runOnNextLateUpdate != null)
        {
            runOnNextLateUpdate.Invoke();
            runOnNextLateUpdate = null;
        }
    }
}
