﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Options<T> : ScriptableObject where T : ScriptableObject
{

    public static string RootAssetPath {
        get {
            return "Assets/resources/options/";
        }
    }

    protected static T _instance;
    private static bool isQuitting;

    public static T Instance
    {
        get
        {
            if (_instance == null) _instance = LoadInstanceFromPrefab();
            return _instance;
        }
    }

    public static T LoadInstanceFromPrefab()
    {
        if (Application.isPlaying && !isQuitting)
        {
            var asset = Resources.Load<ScriptableObject>(AssetResourcePath);
            if (asset == null)
            {
                Debug.LogError("No asset found to instantiate " + typeof(T) + ". Create one from the Options editor window.");
                return null;
            }

            var so = Instantiate(asset) as T;

            return so;
        }
        else
        {
#if UNITY_EDITOR
            var editorInstance = Resources.Load<T>(AssetResourcePath);
            return editorInstance;
#else
            Debug.LogError("Tried to create Instance while in Editor or while shutting down. No bueno.");
            return null;
#endif
        }
    }


    static string AssetResourcePath
    {
        get
        {
            return "options/" + typeof(T).Name;
        }
    }

    // Instance methods

    protected virtual void Awake()
    {
        _instance = this as T;
    }

    void OnApplicationQuit()
    {
        isQuitting = true;
        DestroyInstance();
    }

    void DestroyInstance()
    {
        if (_instance != null) DestroyImmediate(_instance);
    }
}