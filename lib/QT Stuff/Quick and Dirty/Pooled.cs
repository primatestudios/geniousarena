﻿using UnityEngine;
using System.Collections.Generic;
namespace QuickAndDirty {
public class Pooled : MonoBehaviour {
    
    private GameObject _ancestor = null;
    
    private static Dictionary<GameObject, List<GameObject>> _pools = null;
    
    void OnDestroy()
    {
        UnPoolObject(this);
    }
    
    public static void UnPoolObject(Pooled pooled)
    {
        if (_pools.ContainsKey(pooled._ancestor))
        {
            _pools[pooled._ancestor].Remove(pooled.gameObject);
        }
    }
    
    public static GameObject GetPooledObject(GameObject prefabRef)
    {
        TouchPool(prefabRef);
        
        GameObject go;
        if (_pools[prefabRef].Count > 0)
        {
            // I have pooled objects you can have!
            go = _pools[prefabRef][0];
            _pools[prefabRef].RemoveAt(0);
            return go;
        }
        else {
            // Oops, here's a shiny new one instead.
            go = GameObject.Instantiate(prefabRef);
            go.AddComponent<Pooled>()._ancestor = prefabRef;
        }
        return go;
    }
    
    public static void PoolObject(GameObject go)
    {
        var script = go.GetComponent<Pooled>();
        
        if (script == null)
        {
            Debug.LogError(string.Format("{0} is not a pooled object", go.name));
        }
        else {
            TouchPool(script._ancestor);
            _pools[script._ancestor].Add(go);
        }
        
    }
    
    private static void TouchPool(GameObject prefabRef)
    {
        if (_pools == null)
        {
            _pools = new Dictionary<GameObject, List<GameObject>>();
        }
        
        if (!_pools.ContainsKey(prefabRef))
        {
            _pools.Add(prefabRef, new List<GameObject>());
        }
    }
    
    public static void ReleasePool(GameObject prefabRef)
    {
        if (_pools != null)
        {
            if (_pools.ContainsKey(prefabRef))
            {
                _pools.Remove(prefabRef);
                System.GC.Collect();
            }
        }
    }
    
    public static void ReleaseAllPools()
    {
        if (_pools != null)
        {
            _pools.Clear();
            System.GC.Collect();
        }
    }
}
public static class PoolingExtensions
{
    public static GameObject Deploy(this MonoBehaviour m, GameObject prefab, string broadcast = "")
    {
        var go = Pooled.GetPooledObject(prefab);
        if (broadcast != "")
            go.BroadcastMessage(broadcast);
            
        go.SetActive(true);
        return go;
    }
    
    public static GameObject Deploy<T>(this MonoBehaviour m, GameObject prefab, string broadcast, T param)
    {
        var go = Pooled.GetPooledObject(prefab);
        go.BroadcastMessage(broadcast, param);
        
        go.SetActive(true);
        return go;
    }
    
    public static GameObject Deploy(this MonoBehaviour m, GameObject prefab, Vector3 position, Quaternion rotation,
        string broadcast = "")
    {
        var go = Pooled.GetPooledObject(prefab);
        go.transform.position = position;
        go.transform.rotation = rotation;
        
        if (broadcast != "")
            go.BroadcastMessage(broadcast);
        
        go.SetActive(true);
        return go;
    }
    
    public static GameObject Deploy<T>(this MonoBehaviour m, GameObject prefab, Vector3 position, Quaternion rotation,
        string broadcast, T param)
    {
        var go = Pooled.GetPooledObject(prefab);
        go.transform.position = position;
        go.transform.rotation = rotation;
        
        go.BroadcastMessage(broadcast, param);
        
        go.SetActive(true);
        return go;
    }
    
    public static void Recycle(this MonoBehaviour m, GameObject sceneObject)
    {
        Pooled.PoolObject(sceneObject);
        sceneObject.SetActive(false);
    }
}

}
