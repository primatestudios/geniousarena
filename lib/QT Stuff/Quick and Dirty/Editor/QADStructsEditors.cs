﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
namespace QuickAndDirty
{

    [CustomPropertyDrawer(typeof(SceneInfo))]
    public class SceneInfoPropertyDrawer : PropertyDrawer {
		float height = 0f;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
            var sceneProperty = property.FindPropertyRelative("sceneAsset");
            var nameProperty = property.FindPropertyRelative("name");
            height = 0f;

            EditorGUI.BeginProperty(position, label, property);
            //position.y += ;
            position.height = EditorGUI.GetPropertyHeight(sceneProperty);
            EditorGUI.PropertyField(position, sceneProperty);

            height += EditorGUI.GetPropertyHeight(sceneProperty);

            var sceneAsset = sceneProperty.objectReferenceValue as SceneAsset;
            if (sceneAsset != null) {
                nameProperty.stringValue = sceneAsset.name;

                var scenes = EditorBuildSettings.scenes;
                bool contains = false;
                string scenePath = AssetDatabase.GetAssetPath(sceneAsset);
                foreach (var s in EditorBuildSettings.scenes)
                {
                    if (s.path == scenePath) {
                        contains = true;
                        break;
                    }
                }

                if (!contains) {
                    position.y += EditorGUI.GetPropertyHeight(sceneProperty);
                    position.height = 24;
					height += 24;
                    if (GUI.Button(position, new GUIContent(string.Format("Add to build settings: {0}", sceneAsset.name)))) {
                        
                        List<EditorBuildSettingsScene> sceneList = new List<EditorBuildSettingsScene>(scenes);
                        GUID sceneGUID = new GUID(AssetDatabase.AssetPathToGUID(scenePath));
                            sceneList.Add(new EditorBuildSettingsScene() { path = scenePath, enabled = true, guid = sceneGUID});
                        EditorBuildSettings.scenes = sceneList.ToArray();
                    }
                }
            }
            else {
                nameProperty.stringValue = "";
            }
			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
            return height;
		}
	}
}