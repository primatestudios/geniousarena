﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEditor;

namespace QuickAndDirty.Editor {


    public static class SceneEx
    {
        public static string GetGUID(this SceneAsset scene) {
            var path = AssetDatabase.GetAssetPath(scene);
            return AssetDatabase.AssetPathToGUID(path);
        }
    }

}