using UnityEngine;
using System;
using System.Xml.Serialization;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace QuickAndDirty
{
	/*
	 * Helpers for cramming in-game data (ie inspector-exposed structs/classes) into PlayerPrefs and getting it back out again.
	 */
    public class Data
    {
		/*
		 * Fetch a string from PlayerPrefs and try to deserialize it.
		 */
        public static T Read<T>(string key) where T : class
        {
            string s = PlayerPrefs.GetString(key, "");

            if (s.Length < 1) return null;

            StringReader reader = new StringReader(s);
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            return serializer.Deserialize(reader) as T;
        }

		/*
		 * Serialize and cram an object into player prefs. 
		 * 
		 * PlayerPrefs strings have a limit of 1MB. If you're going over that you should look elsewhere for your data storage needs.
		 */
        public static void Write<T>(string key, T data) where T : class
        {
            StringWriter writer = new StringWriter();
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            serializer.Serialize(writer, data);

            string s = writer.ToString();

            PlayerPrefs.SetString(key, s);
        }

        public static void WriteCompressed(string key, object data)
        {
            using (MemoryStream stream = new MemoryStream()) {
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, data);
                using (DeflateStream compression = new DeflateStream(stream, CompressionMode.Compress))
                {
                    string s = Convert.ToBase64String(stream.GetBuffer());

                    if (s.Length > 400000) {
                        Debug.LogWarningFormat("Data \"{0}\" is very large. Consider storing it outside of playerprefs.", key);
                    }

                    PlayerPrefs.SetString(key, s);
                }
            }
        }

        public static T ReadCompressed<T>(string key) where T : class
        {
            string s = PlayerPrefs.GetString(key, "");

            if (s.Length < 1) return null;

            using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(s))) {
                using (DeflateStream decompression = new DeflateStream(stream, CompressionMode.Decompress)) {
                    BinaryFormatter formatter = new BinaryFormatter();

                    try {
                    return (T)formatter.Deserialize(stream);
                    }
                    catch (Exception ex) {
                        Debug.LogErrorFormat("Failed to deserialize {0} stream.", typeof(T).Name);
                        return null;
                    }
                }
            }
        }

		/*
		 * It's a little silly to the normal C# programmer, but this is a dependable and fast way of deep copying serializable objects.
		 */
        public static T Clone<T>(T original)
        {
            // GET OFF MY LAWN~!
            if (!original.GetType().IsSerializable)
            {
                throw new ArgumentException("Type must be serializable.", "original");
            }
            // null is serializable so if someone tries to clone a null reference return the default (which is usually null)
            if (System.Object.ReferenceEquals(original, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream memStream = new MemoryStream()) // Hint to GC that this can be released ASAP
            {
                // Serialize it into a chunk of memory, rewind to the head of that stream, and deserialize it.
                // BPDS
                formatter.Serialize(memStream, original); 
                memStream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(memStream);
            }
        }

        public static T ReadFile<T>(string path) where T : class
        {
            string s = File.ReadAllText(path);

            if (s.Length < 1) return null;

            StringReader reader = new StringReader(s);
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            return serializer.Deserialize(reader) as T;
        }

        public static void WriteFile<T>(string path, T data) where T : class
        {
            StringWriter writer = new StringWriter();
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            serializer.Serialize(writer, data);

            string s = writer.ToString();

            File.WriteAllText(path, s);
        }

        public static void WriteCompressedFile(string path, object data)
        {
            byte[] cerealData = new byte[0];
            
            using (MemoryStream stream = new MemoryStream()) {
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, data);
                cerealData = stream.ToArray();

                File.WriteAllBytes(path + "u", cerealData); // Uncomment this to get a decompressed copy of your file
                stream.Close();

            }
            using (MemoryStream compressStream = new MemoryStream()) {
                using (DeflateStream compression = new DeflateStream(compressStream, CompressionMode.Compress))
                {
                    //string s = Convert.ToBase64String(stream.GetBuffer());
                    compression.Write(cerealData, 0, cerealData.Length);
                    compression.Close();

                    File.WriteAllBytes(path, compressStream.ToArray());
                }
            }
        }

        public static T ReadCompressedFile<T>(string path) where T : class
        {
            var bytes = File.ReadAllBytes(path);

            if (bytes.Length < 1) return null;

            using (MemoryStream stream = new MemoryStream(bytes)) {
                Debug.LogFormat("Compressed Stream Len {0}", stream.Length);
                using (DeflateStream decompressionStream = new DeflateStream(stream, CompressionMode.Decompress)) {
                    decompressionStream.Flush();

                    byte[] bigBytes = new byte[1024];
                    
					using (MemoryStream decompressedStream = new MemoryStream()) {
                        while (true)
                        {
                            int read = decompressionStream.Read(bigBytes, 0, bigBytes.Length);
                            if (read > 0)
                                decompressedStream.Write(bigBytes, 0, read);
                            else
                                break;
                        }

                        decompressedStream.Position = 0;
                        BinaryFormatter formatter = new BinaryFormatter();
                        Debug.LogFormat("Decompressed Stream Len {0}", decompressedStream.Length);

                        try
                        {
                            return (T)formatter.Deserialize(decompressedStream);
                        }
                        catch (Exception ex)
                        {
                            Debug.LogErrorFormat("Failed to deserialize {0} stream.", typeof(T).Name);
                            return null;
                        }
                    }
                }
            }
        }
    }
#if UNITY_EDITOR
    public class EditorData {
        /*
         * EditorPrefs version of Read/Write
         */
        public static T Read<T>(string key) where T : class
        {
            string s = EditorPrefs.GetString(key, "");

            if (s.Length < 1) return null;

            StringReader reader = new StringReader(s);
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            return serializer.Deserialize(reader) as T;
        }

        public static T ReadAsset<T>(string path) where T : class
        {
            string s = File.ReadAllText(path);

            if (s.Length < 1) return null;

            StringReader reader = new StringReader(s);
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            return serializer.Deserialize(reader) as T;
        }

        public static void Write<T>(string key, T data) where T : class
        {
            StringWriter writer = new StringWriter();
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            serializer.Serialize(writer, data);

            string s = writer.ToString();

            EditorPrefs.SetString(key, s);
        }

        public static void WriteAsset<T>(string path, T data) where T : class
        {
            StringWriter writer = new StringWriter();
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            serializer.Serialize(writer, data);

            string s = writer.ToString();

            File.WriteAllText(path, s);
        }
    }
#endif
}
