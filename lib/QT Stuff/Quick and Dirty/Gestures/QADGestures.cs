using UnityEngine;

namespace QuickAndDirty.Gestures {

    public class QADGestures : MonoBehaviour
    {
        public static QADGestures instance;

        public int maxFingers = 5;
        public float dragHoldTime = .15f;
        public Vector2 minDragDelta = new Vector2(1, 1);
        public float maxFlickTime = .3f;
        public float longHoldTime = .5f;
        public Vector2 minFlickDelta = new Vector2(2, 2);

        //Gestures
        Gesture[] fingers;

        //Fake Gesture : ie Mouse
        public bool mouseFakeFinger = false;
        int fakeFingerCount = 0;

        /*
            * This way of doing it makes for a hideous read, BUT it makes the exposed
            * events easy to work with. *Shrug* 
            */
        #region event mutators

        private StartGesture onGestureStart;
        public static event GestureEvent OnGestureStart
        {
            add
            {
                //Would it be bad to just automatically instantiate the prefab if it's missing?
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onGestureStart.OnGestureEvent += value;
            }
            remove
            {
                instance.onGestureStart.OnGestureEvent -= value;
            }
        }
        private HoldGesture onGestureHold;
        public static event GestureEvent OnGestureHold
        {
            add
            {
                //Would it be bad to just automatically instantiate the prefab if it's missing?
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onGestureHold.OnGestureEvent += value;
            }
            remove
            {
                instance.onGestureHold.OnGestureEvent -= value;
            }
        }
        private EndGesture onGestureEnd;
        public static event GestureEvent OnGestureEnd
        {
            add
            {
                //Would it be bad to just automatically instantiate the prefab if it's missing?
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onGestureEnd.OnGestureEvent += value;
            }
            remove
            {
                instance.onGestureEnd.OnGestureEvent -= value;
            }
        }

        private DragStartGesture onDragStartRecognizer;
        public static event GestureEvent OnDragStart
        {
            add
            {
                //Would it be bad to just automatically instantiate the prefab if it's missing?
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onDragStartRecognizer.OnGestureEvent += value;
            }
            remove
            {
                instance.onDragStartRecognizer.OnGestureEvent -= value;
            }
        }

        private DragGesture onDragRecognizer;
        public static event GestureEvent OnDrag
        {
            add
            {
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onDragRecognizer.OnGestureEvent += value;
            }
            remove
            {
                instance.onDragRecognizer.OnGestureEvent -= value;
            }
        }

        private DragEndGesture onDragEndRecognizer;
        public static event GestureEvent OnDragEnd
        {
            add
            {
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onDragEndRecognizer.OnGestureEvent += value;
            }
            remove
            {
                instance.onDragEndRecognizer.OnGestureEvent -= value;
            }
        }
        private DragAndHoldStartGesture onDragHoldStartRecognizer;
        public static event GestureEvent OnDragHoldStart
        {
            add
            {
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onDragHoldStartRecognizer.OnGestureEvent += value;
            }
            remove
            {
                instance.onDragHoldStartRecognizer.OnGestureEvent -= value;
            }
        }
        private DragAndHoldGesture onDragAndHoldRecognizer;
        public static event GestureEvent OnDragAndHold
        {
            add
            {
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onDragAndHoldRecognizer.OnGestureEvent += value;
            }
            remove
            {
                instance.onDragAndHoldRecognizer.OnGestureEvent -= value;
            }
        }
        private DragHoldEndGesture onDragHoldEndRecognizer;
        public static event GestureEvent OnDragHoldEnd
        {
            add
            {
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onDragHoldEndRecognizer.OnGestureEvent += value;
            }
            remove
            {
                instance.onDragHoldEndRecognizer.OnGestureEvent -= value;
            }
        }
        private TapGesture onTapRecognizer;
        public static event GestureEvent OnTap
        {
            add
            {
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onTapRecognizer.OnGestureEvent += value;
            }
            remove
            {
                instance.onTapRecognizer.OnGestureEvent -= value;
            }
        }
        private LongHoldGesture onLongHoldRecognizer;
        public static event GestureEvent OnLongHold
        {
            add
            {
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onLongHoldRecognizer.OnGestureEvent += value;
            }
            remove
            {
                instance.onLongHoldRecognizer.OnGestureEvent -= value;
            }
        }
        private LongHoldEndGesture onLongHoldEndRecognizer;
        public static event GestureEvent OnLongHoldEnd
        {
            add
            {
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onLongHoldEndRecognizer.OnGestureEvent += value;
            }
            remove
            {
                instance.onLongHoldEndRecognizer.OnGestureEvent -= value;
            }
        }

        private FlickGesture onFlickGestureRecognizer;
        public static event GestureEvent OnFlickEvent
        {
            add
            {
                if (instance == null)
                {
                    Debug.Log("QADGestures instance is null, did you forget to add a QADGestures component to the scene?");
                    return;
                }
                instance.onFlickGestureRecognizer.OnGestureEvent += value;
            }
            remove
            {
                instance.onFlickGestureRecognizer.OnGestureEvent -= value;
            }
        }
        #endregion

        void Awake()
        {
            if (instance != null)
            {
                Object.DestroyImmediate(this);
                return;
            }
            else
            {
                instance = this;
            }

            /*
                * Mouse inputs and touch inputs overlap, so on devices with actual 
                * touch screens the "fake finger" needs to be diabled.
                */
    #if !UNITY_EDITOR
        mouseFakeFinger = false;
    #endif


            //Initialize recognizers
            onGestureStart = new StartGesture();
            onGestureHold = new HoldGesture();
            onGestureEnd = new EndGesture();

            onDragStartRecognizer = new DragStartGesture();
            onDragRecognizer = new DragGesture();
            onDragEndRecognizer = new DragEndGesture();

            onDragHoldStartRecognizer = new DragAndHoldStartGesture();
            onDragAndHoldRecognizer = new DragAndHoldGesture();
            onDragHoldEndRecognizer = new DragHoldEndGesture();

            onLongHoldRecognizer = new LongHoldGesture();
            onLongHoldEndRecognizer = new LongHoldEndGesture();

            onTapRecognizer = new TapGesture();
            onFlickGestureRecognizer = new FlickGesture();

            //Setup fingers
            if (mouseFakeFinger)
            {
                fingers = new Gesture[maxFingers + 1];
                fingers[maxFingers] = new MouseGesture(maxFingers);
                fakeFingerCount = 1;
            }
            else
            {
                fingers = new Gesture[maxFingers];
            }

            for (int i = 0; i < maxFingers; ++i)
                fingers[i] = new Gesture(i);
        }


        void Update()
        {
            //Early exit
            if (Input.touchCount + fakeFingerCount < 1)
            {
                //Debug.Log("Early exit");
                return;
            }



            //Update Fingers that unity considers active (I ain't gonna argue with em)
            foreach (Touch t in Input.touches)
            {
                int id = t.fingerId;
                if (id < maxFingers)
                {
                    fingers[id].UpdateGesture(t);
                }
            }

            //Update the fake finger
            if (mouseFakeFinger)
            {
                MouseGesture m = fingers[maxFingers] as MouseGesture;
                m.UpdateMouse();
                fingers[maxFingers] = m as Gesture;
            }

            //Run active fingers through recognizers
            //Another hideous read to avoid virtual functions
            onGestureStart.UpdateRecognizer(fingers);
            onGestureHold.UpdateRecognizer(fingers);
            onGestureEnd.UpdateRecognizer(fingers);

            onDragStartRecognizer.UpdateRecognizer(fingers);
            onDragRecognizer.UpdateRecognizer(fingers);
            onDragEndRecognizer.UpdateRecognizer(fingers);

            onDragHoldStartRecognizer.UpdateRecognizer(fingers);
            onLongHoldRecognizer.UpdateRecognizer(fingers);
            onLongHoldEndRecognizer.UpdateRecognizer(fingers);

            onDragAndHoldRecognizer.UpdateRecognizer(fingers);
            onDragHoldEndRecognizer.UpdateRecognizer(fingers);

            onFlickGestureRecognizer.UpdateRecognizer(fingers);
            onTapRecognizer.UpdateRecognizer(fingers);

            //After all recognizers run, cleanup "ended" touches
            foreach (Gesture finger in fingers)
            {
                if (finger.phase == TouchPhase.Ended || finger.phase == TouchPhase.Canceled)
                    finger.dirty = true;
            }

            //*waves hand* This is not the code you're looking for...
            //if (mouseFakeFinger)
            //{
            //    if (fingers[maxFingers].phase == TouchPhase.Ended)
            //    {
            //        fingers[maxFingers] = null;
            //        fakeFingerCount = 0;
            //    }
            //}
        }
    }
}