﻿using UnityEngine;

namespace QuickAndDirty.Gestures {
    public class LongHoldGesture : GestureRecognizer
    {
        bool[] started;

        public float longHoldTime;
        public Vector2 maxDelta;

        public LongHoldGesture()
            : base()
        {
            longHoldTime = QADGestures.instance.longHoldTime;
            maxDelta = QADGestures.instance.minDragDelta;
            started = new bool[QADGestures.instance.maxFingers + 1];
        }

        public override bool Recognize(Gesture hold)
        {
            if (!started[hold.fingerID])
            {

                if (hold.totalDeltaPosition.sqrMagnitude <= maxDelta.sqrMagnitude
                    && hold.timeHeld >= longHoldTime)
                {
                    //Set finger started to true, and set up a delegate to retrun to false
                    started[hold.fingerID] = true;

                    GestureEvent foo = null;
                    foo = delegate(Gesture g)
                    {
                        started[g.fingerID] = false;
                        QADGestures.OnLongHoldEnd -= foo;
                    };

                    QADGestures.OnLongHoldEnd += foo;

                    return true;
                }
            }

            return false;
        }
    }

}