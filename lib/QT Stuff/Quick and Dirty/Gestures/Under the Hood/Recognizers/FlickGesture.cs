using UnityEngine;
namespace QuickAndDirty.Gestures {
    public class FlickGesture : GestureRecognizer
    {
        public float flickTime;
        public Vector2 minDelta;

        public FlickGesture()
            : base()
        {
            flickTime = QADGestures.instance.maxFlickTime;
            minDelta = QADGestures.instance.minFlickDelta;
        }

        public override bool Recognize(Gesture touch)
        {
            return touch.phase == TouchPhase.Ended
                && touch.totalDeltaPosition.sqrMagnitude > minDelta.sqrMagnitude
                && touch.timeHeld < flickTime;
        }
    }
}
