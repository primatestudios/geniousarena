using UnityEngine;
namespace QuickAndDirty.Gestures {
    public class HoldGesture : GestureRecognizer
    {
        public float dragHoldTime;
        public Vector2 minDelta;

        public HoldGesture()
            : base()
        {
            //dragHoldTime = QADGestures.instance.dragHoldTime;
            //minDelta = QADGestures.instance.minDragDelta;
        }

        public override bool Recognize(Gesture gesture)
        {
            //Debug.Log(gesture.phase + " " + gesture.deltaPosition + " " + gesture.timeHeld);
            return gesture.phase != TouchPhase.Ended;
        }
    }

}