using UnityEngine;

namespace QuickAndDirty.Gestures {
    public class DragStartGesture : GestureRecognizer
    {
        bool[] started;

        public float dragHoldTime;
        public Vector2 minDelta;



        public DragStartGesture()
            : base()
        {
            dragHoldTime = QADGestures.instance.dragHoldTime;
            minDelta = QADGestures.instance.minDragDelta;
            started = new bool[QADGestures.instance.maxFingers + 1];
        }

        public override bool Recognize(Gesture gesture)
        {
            if (!started[gesture.fingerID])
            {
                if (gesture.totalDeltaPosition.sqrMagnitude > minDelta.sqrMagnitude
                    && gesture.timeHeld > dragHoldTime)
                {
                    //Set finger started to true, and set up a delegate to return false
                    started[gesture.fingerID] = true;

                    GestureEvent foo = null;
                    foo = delegate(Gesture g)
                    {
                        started[g.fingerID] = false;
                        QADGestures.OnDragEnd -= foo;
                    };

                    QADGestures.OnDragEnd += foo;

                    return true;
                }
            }

            return false;
        }
    }
}