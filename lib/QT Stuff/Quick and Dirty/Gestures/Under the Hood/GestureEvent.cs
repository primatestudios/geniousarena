﻿    /*
     * You can argue all you want, or you can just admit that delegates are weird.
     */
namespace QuickAndDirty.Gestures {
    public delegate void GestureEvent(Gesture touchInfo);
}
