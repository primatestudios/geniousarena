using UnityEngine;
using System.Collections;
using QuickAndDirty.Gestures;

public class FingerIdGUI : MonoBehaviour {
	
	float width = 128f;
	float height = 64f;
	
	Gesture[] fingers;
	
	// Use this for initialization
	void Start () 
	{
		fingers = new Gesture[QADGestures.instance.maxFingers + 1];
		
		for(int i = 0; i < fingers.Length; ++i)
			fingers[i] = new Gesture(i);
		
		QADGestures.OnDragAndHold += delegate(Gesture touchInfo) {
			fingers[touchInfo.fingerID] = touchInfo;
		};
	}
	
	// Update is called once per frame
	void OnGUI () {
		GUI.skin.box.fontSize = 18;
		GUI.backgroundColor = Color.green;
		for(int i = 0; i < fingers.Length; ++i)
		{
			if(!fingers[i].dirty && fingers[i] != null)
			{
				
				Rect boxRect = new Rect(fingers[i].position.x - width*.5f, Screen.height - fingers[i].position.y - height*.5f, width, height);
				GUI.Box(boxRect, i + " = " + fingers[i].fingerID + "\n" + fingers[i].phase + "\n" + fingers[i].position);
			}
		}
//		GUI.backgroundColor = Color.red;
//		foreach(Touch t in Input.touches)
//		{
//			
//			Rect boxRect = new Rect(t.position.x - width*.5f, Screen.height - t.position.y - height*.5f, width, height);
//			GUI.Box(boxRect, t.fingerId + "\n" + t.phase + "\n" + t.position);
//		}
	}
}
