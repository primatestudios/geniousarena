// GENERATED AUTOMATICALLY FROM 'Assets/GeniousArena/Input/FighterControls.inputactions'

using System;
using UnityEngine;
using UnityEngine.Experimental.Input;


[Serializable]
public class FighterControls : InputActionAssetReference
{
    public FighterControls()
    {
    }
    public FighterControls(InputActionAsset asset)
        : base(asset)
    {
    }
    private bool m_Initialized;
    private void Initialize()
    {
        // Player
        m_Player = asset.GetActionMap("Player");
        m_Player_Special = m_Player.GetAction("Special");
        m_Player_Jab = m_Player.GetAction("Jab");
        m_Player_Jump = m_Player.GetAction("Jump");
        m_Player_Smash = m_Player.GetAction("Smash");
        m_Player_Grab = m_Player.GetAction("Grab");
        m_Player_Block = m_Player.GetAction("Block");
        m_Player_MoveForward = m_Player.GetAction("MoveForward");
        m_Player_Target = m_Player.GetAction("Target");
        m_Player_Drop = m_Player.GetAction("Drop");
        m_Player_ThrowForward = m_Player.GetAction("ThrowForward");
        m_Player_ThrowUp = m_Player.GetAction("ThrowUp");
        m_Player_DownAir = m_Player.GetAction("DownAir");
        m_Player_UpAir = m_Player.GetAction("UpAir");
        m_Player_NeutralAir = m_Player.GetAction("NeutralAir");
        m_Player_AirDodge = m_Player.GetAction("AirDodge");
        m_Player_ToggleCamera = m_Player.GetAction("ToggleCamera");
        m_Player_Dpad = m_Player.GetAction("Dpad");
        m_Player_ToggleTargetLock = m_Player.GetAction("ToggleTargetLock");
        m_Player_Movement = m_Player.GetAction("Movement");
        m_Player_RegisterController = m_Player.GetAction("RegisterController");
        m_Player_AltAxis = m_Player.GetAction("AltAxis");
        m_Player_UpDownAxis = m_Player.GetAction("UpDownAxis");
        m_Player_GetUp = m_Player.GetAction("GetUp");
        m_Initialized = true;
    }
    private void Uninitialize()
    {
        m_Player = null;
        m_Player_Special = null;
        m_Player_Jab = null;
        m_Player_Jump = null;
        m_Player_Smash = null;
        m_Player_Grab = null;
        m_Player_Block = null;
        m_Player_MoveForward = null;
        m_Player_Target = null;
        m_Player_Drop = null;
        m_Player_ThrowForward = null;
        m_Player_ThrowUp = null;
        m_Player_DownAir = null;
        m_Player_UpAir = null;
        m_Player_NeutralAir = null;
        m_Player_AirDodge = null;
        m_Player_ToggleCamera = null;
        m_Player_Dpad = null;
        m_Player_ToggleTargetLock = null;
        m_Player_Movement = null;
        m_Player_RegisterController = null;
        m_Player_AltAxis = null;
        m_Player_UpDownAxis = null;
        m_Player_GetUp = null;
        m_Initialized = false;
    }
    public void SetAsset(InputActionAsset newAsset)
    {
        if (newAsset == asset) return;
        if (m_Initialized) Uninitialize();
        asset = newAsset;
    }
    public override void MakePrivateCopyOfActions()
    {
        SetAsset(ScriptableObject.Instantiate(asset));
    }
    // Player
    private InputActionMap m_Player;
    private InputAction m_Player_Special;
    private InputAction m_Player_Jab;
    private InputAction m_Player_Jump;
    private InputAction m_Player_Smash;
    private InputAction m_Player_Grab;
    private InputAction m_Player_Block;
    private InputAction m_Player_MoveForward;
    private InputAction m_Player_Target;
    private InputAction m_Player_Drop;
    private InputAction m_Player_ThrowForward;
    private InputAction m_Player_ThrowUp;
    private InputAction m_Player_DownAir;
    private InputAction m_Player_UpAir;
    private InputAction m_Player_NeutralAir;
    private InputAction m_Player_AirDodge;
    private InputAction m_Player_ToggleCamera;
    private InputAction m_Player_Dpad;
    private InputAction m_Player_ToggleTargetLock;
    private InputAction m_Player_Movement;
    private InputAction m_Player_RegisterController;
    private InputAction m_Player_AltAxis;
    private InputAction m_Player_UpDownAxis;
    private InputAction m_Player_GetUp;
    public struct PlayerActions
    {
        private FighterControls m_Wrapper;
        public PlayerActions(FighterControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Special { get { return m_Wrapper.m_Player_Special; } }
        public InputAction @Jab { get { return m_Wrapper.m_Player_Jab; } }
        public InputAction @Jump { get { return m_Wrapper.m_Player_Jump; } }
        public InputAction @Smash { get { return m_Wrapper.m_Player_Smash; } }
        public InputAction @Grab { get { return m_Wrapper.m_Player_Grab; } }
        public InputAction @Block { get { return m_Wrapper.m_Player_Block; } }
        public InputAction @MoveForward { get { return m_Wrapper.m_Player_MoveForward; } }
        public InputAction @Target { get { return m_Wrapper.m_Player_Target; } }
        public InputAction @Drop { get { return m_Wrapper.m_Player_Drop; } }
        public InputAction @ThrowForward { get { return m_Wrapper.m_Player_ThrowForward; } }
        public InputAction @ThrowUp { get { return m_Wrapper.m_Player_ThrowUp; } }
        public InputAction @DownAir { get { return m_Wrapper.m_Player_DownAir; } }
        public InputAction @UpAir { get { return m_Wrapper.m_Player_UpAir; } }
        public InputAction @NeutralAir { get { return m_Wrapper.m_Player_NeutralAir; } }
        public InputAction @AirDodge { get { return m_Wrapper.m_Player_AirDodge; } }
        public InputAction @ToggleCamera { get { return m_Wrapper.m_Player_ToggleCamera; } }
        public InputAction @Dpad { get { return m_Wrapper.m_Player_Dpad; } }
        public InputAction @ToggleTargetLock { get { return m_Wrapper.m_Player_ToggleTargetLock; } }
        public InputAction @Movement { get { return m_Wrapper.m_Player_Movement; } }
        public InputAction @RegisterController { get { return m_Wrapper.m_Player_RegisterController; } }
        public InputAction @AltAxis { get { return m_Wrapper.m_Player_AltAxis; } }
        public InputAction @UpDownAxis { get { return m_Wrapper.m_Player_UpDownAxis; } }
        public InputAction @GetUp { get { return m_Wrapper.m_Player_GetUp; } }
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled { get { return Get().enabled; } }
        public InputActionMap Clone() { return Get().Clone(); }
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
    }
    public PlayerActions @Player
    {
        get
        {
            if (!m_Initialized) Initialize();
            return new PlayerActions(this);
        }
    }
    private int m_ControllerSchemeIndex = -1;
    public InputControlScheme ControllerScheme
    {
        get

        {
            if (m_ControllerSchemeIndex == -1) m_ControllerSchemeIndex = asset.GetControlSchemeIndex("Controller");
            return asset.controlSchemes[m_ControllerSchemeIndex];
        }
    }
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get

        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.GetControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
}
